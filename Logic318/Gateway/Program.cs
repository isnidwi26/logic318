﻿namespace Gateway;

public class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine("== Welcome to batch 318 ==");
		Console.WriteLine("|   1. Hari 1           |");
		Console.WriteLine("|   2. Hari 2           |");
		Console.WriteLine("|   3. Hari 3           |");
		Console.WriteLine("|   4. Hari 4           |");
		Console.WriteLine("|   5. Hari 5           |");
		Console.WriteLine("|   6. Hari 6           |");
		Console.WriteLine("|   7. Hari 7           |");
		Console.WriteLine("|   8. Hari 8           |");
		Console.WriteLine("|   9. Hari 9           |");
		Console.WriteLine("|   10. Hari 10         |");
		string answer = "t";

		while (answer.ToLower() == "t")
		{
			
			Console.Write("Pilih hari: ");
			int hari = int.Parse(Console.ReadLine());

			switch (hari)
			{
				case 5:
					Logic05.Program program05 = new Logic05.Program();
					break;
				case 6:
					Logic06.Program program06 = new Logic06.Program();
					break;
				case 7:
					Logic07.Program program07 = new Logic07.Program();
					break;
				case 8:
					Logic08.Program program08 = new Logic08.Program();
					break;
				case 9:
					Logic09.Program program09 = new Logic09.Program();
					break;
				case 10:
					Logic10.Program program10 = new Logic10.Program();
					break;
				default:
					Console.WriteLine("Tidak ada dalam menu");
					break;
			}
			Console.WriteLine();
			Console.Write("Keluar? [Y/T]");
			answer = Console.ReadLine();
		}

	}
}