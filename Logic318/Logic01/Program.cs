﻿
//Konversi();
//operatorAritmatika();
//modulus();
//operatorPenugasan();
//operatorPerbandingan();
//operatorLogika();
//hitungAVG();
//hargaBaju();
//ternary();
//manipulasiString();
//splitJoin();
subString();




static void Konversi()
{
	int myInt = 10;
	double myDouble = 5.25;
	bool myBool = false;

	Console.WriteLine("Convert Int to String: " + Convert.ToString(myInt));
	Console.WriteLine("Convert Int to String: " + myInt.ToString());
	Console.WriteLine("Convert Int to Double: " + Convert.ToDouble(myInt));
	Console.WriteLine("Convert Double to Int: " + Convert.ToInt32(myDouble));
	Console.WriteLine("Convert Boolean to String: " + Convert.ToString(myBool));
}

static void operatorAritmatika()
{
	int mangga, apel, hasil = 0;

	Console.WriteLine("-- Opertor Aritmatika --");
	Console.Write("Masukkan jumlah mangga: ");
	mangga = int.Parse(Console.ReadLine());
	Console.Write("Masukkan jumlah apel: ");
	apel = int.Parse(Console.ReadLine());
	hasil = mangga + apel;
	Console.WriteLine($"Total buah yang anda punya sebanyak {hasil} buah");
}

static void modulus()
{
	int mangga, apel, hasil = 0;

	Console.WriteLine("-- Opertor Aritmatika --");
	Console.Write("Masukkan jumlah mangga: ");
	mangga = int.Parse(Console.ReadLine());
	Console.Write("Masukkan jumlah apel: ");
	apel = int.Parse(Console.ReadLine());
	hasil = mangga % apel;
	Console.WriteLine($"Total mangga % apel : {hasil} buah");
}

static void operatorPenugasan()
{
	int mangga = 10;
	int apel = 8;

	//isi ulang variable
	mangga = 15;
	apel += 6;

	Console.WriteLine("mangga: " + mangga);
	Console.WriteLine("apel: " + apel);
}

static void operatorPerbandingan()
{
	int anggur, kiwi = 0;
	Console.WriteLine("-- Operator Perbandingan --");
	Console.Write("Jumlah anggur: ");
	anggur = int.Parse(Console.ReadLine());
	Console.Write("Jumlah kiwi: ");
	kiwi = int.Parse(Console.ReadLine());

	Console.WriteLine("Hasil perbandingan: ");
	Console.WriteLine($"Anggur > kiwi : {anggur > kiwi}");
	Console.WriteLine($"Anggur < kiwi : {anggur < kiwi}");
	Console.WriteLine($"Anggur <= kiwi : {anggur <= kiwi}");
	Console.WriteLine($"Anggur >= kiwi : {anggur >= kiwi}");
	Console.WriteLine($"Anggur != kiwi : {anggur != kiwi}");
	Console.WriteLine($"Anggur == kiwi : {anggur == kiwi}");

}

static void operatorLogika()
{
	Console.WriteLine("-- Operator Logika -- ");
	Console.Write("masukkan umur: ");
	int umur = int.Parse(Console.ReadLine());
	Console.Write("Masukkan password= ");
	string pass = Console.ReadLine();

	bool isAdult = umur > 18;
	bool isPasswordValid = pass == "admin";
	if(isAdult && isPasswordValid)
	{
		Console.WriteLine("Anda sudah dewasa dan password valid");
	}else if(isAdult && !isPasswordValid)
	{
		Console.WriteLine("Anda sudah dewasa dan password invalid");
	}
	else if(!isAdult && isPasswordValid)
	{
		Console.WriteLine("Anda belum dewasa dan password valid");
	}
	else
	{
		Console.WriteLine("anda belum dewasa dan anda password invalid");
	}

}

static void hitungAVG()
{
	Console.Write("Masukkan nilai MTK : ");
	int mtk = int.Parse(Console.ReadLine());
	Console.Write("Masukkan nilai Fisika: ");
	int fisika = int.Parse(Console.ReadLine());
	Console.Write("Masukkan nilai kimia: ");
	int kimia = int.Parse(Console.ReadLine());
	double avg = 0;
	avg = (mtk+fisika+kimia)/3;
	Console.WriteLine("Nilai rata-rata: " + avg);

	if (avg >= 50)
	{
		Console.WriteLine("Selamat\nKamu berhasil\nKamu hebat");
	}
	else
	{
		Console.WriteLine("Maaf\nKamu gagal");
	}

}

static void hargaBaju()
{
	Console.Write("Masukkan kode baju = ");
	int kode = int.Parse(Console.ReadLine());
	Console.Write("Masukkan kode ukuran = ");
	char ukuran = char.Parse(Console.ReadLine().ToLower());

	int harga = 0;
	string merk = "";

	if(kode >= 1 && kode <= 3)
	{
		if (kode == 1)
		{
			merk = "IMP";
			if (ukuran == 's')
				harga = 200000;
			else if (ukuran == 'm')
				harga = 2200000;
			else
				harga = 250000;
		}
		else if (kode == 2)
		{
			merk = "Prada";
			if (ukuran == 's')
				harga = 150000;
			else if (ukuran == 'm')
				harga = 160000;
			else
				harga = 170000;
		}
		else if (kode == 3)
		{
			merk = "Gucci";
			harga = 200000;
		}
		Console.WriteLine("Merk Baju = " + merk);
		Console.WriteLine("Harga = " + harga);
	}
	else
	{
		Console.WriteLine("Inputan salah!.");
	}

}

static void ternary()
{
	Console.WriteLine("-- Ternary --");
	int x, y;
	Console.Write("Masukkan nilai x: ");
	x = int.Parse(Console.ReadLine());
	Console.Write("Masukkan nilai y: ");
	y = int.Parse(Console.ReadLine());

	string hasilTernary = x > y ? "Nilai x lebih besar dari nilai y" : "Nilai x lebih kecil dari nilai y";
	Console.WriteLine(hasilTernary);
}

static void manipulasiString()
{
	Console.WriteLine("--Manipulasi String--");
	Console.Write("Masukkan kata: ");
	string kata = Console.ReadLine();

	string remove = kata;
	string insert = kata;
	string replace = kata;

	Console.WriteLine("Panjang karakter dari " + kata + " sebanyak " + kata.Length);
	Console.WriteLine($"ToUpper dari {kata} = {kata.ToUpper()}");
	Console.WriteLine($"ToLower dari {kata} = {kata.ToLower()}");
	Console.WriteLine($"Remove dari {remove} = {kata.Remove(11)}");
	Console.WriteLine($"Insert dari {insert} = {kata.Insert(5,"BANGET")}");
	Console.WriteLine($"Replace dari {replace} = {kata.Replace("id","net")}");
}

static void splitJoin()
{
	Console.WriteLine("--Split dan Join--");
	Console.Write("Masukkan kalimat: ");
	string kalimat = Console.ReadLine();
	string[] kataArray = kalimat.Split(" ");

	foreach( string kata in kataArray)
	{
		Console.WriteLine(kata);
	}

	//Join
	Console.WriteLine(string.Join("+", kataArray));
}

static void subString()
{
	Console.WriteLine("--SubString--");
	Console.Write("Masukkan kode: ");
	string kode = Console.ReadLine();

	Console.WriteLine($"String tahun = {kode.Substring(1, 4)}");
	Console.WriteLine($"String bulan = {kode.Substring(5, 2)}");
	Console.WriteLine($"Kode lokasi = {kode.Substring(7, 2)}");
	Console.WriteLine($"Nomor = {kode.Substring(9)}");
}