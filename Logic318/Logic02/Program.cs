﻿using Logic02;

//switchState();
//loopingWhile();
//loopingDoWhile();
//loopingForIncrement();
//loopingForDecrement();
//forBersarang();
//arrayForeach();
//inputArray();
//array2Dimensi();
//inisialisasiList();
//listClass();
//listAdd();
//listRemove();
//listInsert();
//convertArrayAll();
contains();


static void contains()
{
	Console.WriteLine("--Contain--");
	Console.Write("Masukkan kalimat: ");
	string kalimat = Console.ReadLine();
	Console.Write("Masukkan contain: ");
	string contain = Console.ReadLine();

	if (kalimat.Contains(contain))
	{
		Console.WriteLine($"Kalimat ({kalimat}) mengandung kalimat ({contain})");
	}
	else
	{
		Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung {contain}");
	}
}
static void padLeft()
{
	Console.WriteLine("--Pad Left--");
	Console.Write("Masukkan input: ");
	int input = int.Parse(Console.ReadLine());
	Console.Write("Masukkan panjang karakter: ");
	int panjang = int.Parse(Console.ReadLine());
	Console.Write("Masukkan char: ");
	char chars = char.Parse(Console.ReadLine());

	Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(panjang, chars)}");
}
static void convertArrayAll()
{
	Console.WriteLine("--Convert Array All--");
	Console.Write("Masukkan angka array (pakai koma): ");
	int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

	Console.WriteLine(string.Join("\t", array));
}
static void listInsert()
{
	Console.WriteLine("--Contoh List--");
	List<Users> users = new List<Users>()
	{
		new Users() {Name="Astika Fujianti Rahayu", Age=17},
		new Users() {Name="Alfi Azizi", Age=18},
		new Users() {Name="Abdullah Muaffa", Age=18},
		new Users() {Name="Marchelino Marcholippi Sihotang", Age=18},
		new Users() {Name="Laudry Melano Kaiin", Age=18},
	};

	users.Insert(0, new Users() { Name = "Dwi tiniwinibiti", Age = 17 });

	//atau

	Users user = new Users();
	user.Name = "Dwit";
	user.Age = 18;
	users.Insert(5, user);

	for (int i = 0; i < users.Count; i++)
	{
		Console.WriteLine(users[i].Name + " berumur " + users[i].Age + " tahun.");
	}

}
static void listRemove()
{
	Console.WriteLine("--Contoh List--");
	List<Users> users = new List<Users>()
	{
		new Users() {Name="Astika Fujianti Rahayu", Age=17},
		new Users() {Name="Alfi Azizi", Age=18},
		new Users() {Name="Abdullah Muaffa", Age=18},
		new Users() {Name="Marchelino Marcholippi Sihotang", Age=18},
		new Users() {Name="Laudry Melano Kaiin", Age=18},
	};

	users.RemoveAt(0);

	Users user = users.Where( a => a.Name == "Abdullah Muaffa").FirstOrDefault();
	users.Remove(user);

	for (int i = 0; i < users.Count; i++)
	{
		Console.WriteLine(users[i].Name + " berumur " + users[i].Age + " tahun.");
	}

}
static void listAdd()
{
	Console.WriteLine("--Contoh List--");
	List<Users> users = new List<Users>()
	{
		new Users() {Name="Astika Fujianti Rahayu", Age=17},
		new Users() {Name="Alfi Azizi", Age=18},
		new Users() {Name="abdullah Muaffa", Age=18},
		new Users() {Name="Marchelino Marcholippi Sihotang", Age=18},
		new Users() {Name="Laudry Melano Kaiin", Age=18},
	};

	users.Add(new Users() { Name="Isni Dwitiniwinibiti", Age = 17});

	//atau
	Users user = new Users();
	user.Name = "Dwi";
	user.Age = 17;

	for(int i=0; i<users.Count; i++)
	{
		Console.WriteLine(users[i].Name + " berumur " + users[i].Age+ " tahun.");
	}

}
static void listClass()
{
	Console.WriteLine("--Contoh List--");
	List<Users> user = new List<Users>()
	{
		new Users() {Name="Astika Fujianti Rahayu", Age=17},
		new Users() {Name="Alfi Azizi", Age=18},
		new Users() {Name="abdullah Muaffa", Age=18},
		new Users() {Name="Marchelino Marcholippi Sihotang", Age=18},
		new Users() {Name="Laudry Melano Kaiin", Age=17},
	};

	for(int i=0; i<user.Count; i++)
	{
		Console.WriteLine(user[i].Name + " berumur " + user[i].Age+ " tahun.");
	}

}
static void inisialisasiList()
{
	List<string> list = new List<string>()
	{
		"Astika Fujianti Rahayu",
		"Alfi Azizi",
		"Abdullah Muaffa",
		"Marchelino Marcholippi Sihotang"
	};
	list.Add("Laudry Melano Kaiin");
	Console.WriteLine(string.Join(", ",list));
}
static void array2DimensiFor()
{
	int[,] array = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
	for (int i = 0; i < array.GetLength(0); i++)
	{
		for (int j = 0; j < array.GetLength(1); j++)
		{
			Console.WriteLine(array[i, j] + "\t");
		}
		Console.WriteLine();
	}
}
static void array2Dimensi()
{
	int[,] array = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

	Console.WriteLine(array[0,2]);//3
	Console.WriteLine(array[1,1]);//5
	Console.WriteLine(array[1,0]);//4
	Console.WriteLine(array[2,1]);//8
}
static void inputArray()
{
	int[] a = new int[5];
	for(int i=0; i<a.Length; i++)
	{
		Console.Write("Masukkan array ke-"+(i+1)+": ");
		a[i] = int.Parse(Console.ReadLine());
	}
	foreach(int i in a)
	{
		Console.WriteLine(i);
	}
}
static void dekArray()
{
	//Array merupakan variabel yang dapat menampung banyak nilai sekaligus.

	//deklarasi array
	int[] array = new int[3] { 1, 2, 3 };
	int[] arrayB = new int[3];
	arrayB[0] = 1;
	arrayB[1] = 2;
	arrayB[2] = 3;

	string[] arrayString = new string[] {"Alfi", "Muaffa", "Marchel"};
	for(int i  = 0; i < arrayString.Length; i++)
	{
		Console.WriteLine(arrayString[i]);
	}
}
static void arrayForeach()
{
	int[] arrayAngka = { 1, 2, 3, 4 };
	int sum = 0;
	foreach(int i in arrayAngka)
		sum += i;
	
	Console.WriteLine(sum);
}
static void forBersarang()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			Console.Write($"({i}, {j})");
		}
		Console.WriteLine();
	}
}
static void loopingForDecrement()
{
	for(int i = 10; i > 0; i--)
	{
		Console.WriteLine(i);
	}
}
static void loopingForIncrement()
{
	for(int i = 0; i < 10; i++)
	{
		Console.WriteLine(i);
	}
}
static void loopingDoWhile()
{
	int a = 0;
	do
	{
		Console.WriteLine(a); 
		a++;
	} while (a<5);
}
static void loopingWhile()
{
	Console.WriteLine("---Perulangan While---");
	bool ulangi = true;
	int nilai = 1;
	while (ulangi)
	{
		Console.WriteLine("Proses ke: " + nilai);
		nilai++;

		Console.Write("Apakah anda akan mengulangi proses? (y/n) ");
		string input = Console.ReadLine().ToLower();

		if (input == "y")
			ulangi = true;
		else if (input == "n")
			ulangi = false;
		else
		{
			nilai = 1;
			Console.WriteLine("Input yang anda masukkan salah");
			ulangi = true;
		}
	}
}
static void switchState()
{
	Console.WriteLine("Pilih buah kesukaan anda (apel/jeruk/manggis): ");
	string pilihan = Console.ReadLine().ToLower();

	switch (pilihan)
	{
		case "apel":
			Console.WriteLine("anda memilih buah apel");
			break;
		case "jeruk":
			Console.WriteLine("anda memilih buah jeruk");
			break;
		case "manggis":
			Console.WriteLine("anda memilih buah manggis");
			break;
		default:
			Console.WriteLine("anda memilih buah yang lain");
			break;
	}
}

