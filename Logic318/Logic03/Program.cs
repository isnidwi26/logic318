﻿using System.Globalization;
//Break();
//Continue();
//stringToCharArray();
//indexOf();
//datetime();
//datetimeParsing();
//datetimeProperties();
timespan();


static void timespan()
{
	DateTime date1 = new DateTime(2023, 06, 26, 06, 26, 06);
	DateTime date2 = new DateTime(2023, 06, 28, 09, 29, 09);

	TimeSpan selisih = date2 - date1;

	Console.WriteLine($"Total hari				: {selisih.Days}");
	Console.WriteLine($"Total hari				: {selisih.TotalDays}");
	Console.WriteLine($"Total selisih jam			: {selisih.Hours}");
	Console.WriteLine($"Total jam				: {selisih.TotalHours}");
	Console.WriteLine($"Total selisih menit			: {selisih.Minutes}");
	Console.WriteLine($"Total menit				: {selisih.TotalMinutes}");
	Console.WriteLine($"Total selisih detik			: {selisih.Seconds}");
	Console.WriteLine($"Total detik				: {selisih.TotalSeconds}");
	Console.WriteLine($"Total selisih milisecond		: {selisih.Milliseconds}");
	Console.WriteLine($"Total milisecond			: {selisih.TotalMilliseconds}");
	Console.WriteLine($"Total ticks				: {selisih.Ticks}");




}
static void datetimeProperties()
{
	Console.WriteLine("-- Date Time Properties --");
	DateTime date = new DateTime(2023, 5, 4, 11, 1, 45);

	int tahun = date.Year;
	int bulan = date.Month;
	int hari = date.Day;
	int jam = date.Hour;
	int menit = date.Minute;
	int detik = date.Second;
	int weekDay = (int)date.DayOfWeek;
	string hariString = date.ToString("dddd");
	string hariString2 = date.ToString("dddd", new CultureInfo("id-ID"));
	string hariString3 = date.DayOfWeek.ToString();

	Console.WriteLine($"Tahun: {tahun}");
	Console.WriteLine($"Bulan: {bulan}");
	Console.WriteLine($"Hari: {hari}");
	Console.WriteLine($"Jam: {jam}");
	Console.WriteLine($"Menit: {menit}");
	Console.WriteLine($"Detik: {detik}");
	Console.WriteLine($"WeekDay: {weekDay}");
	Console.WriteLine($"HariString: {hariString}");
	Console.WriteLine($"HariString2: {hariString2}");
	Console.WriteLine($"HariString3: {hariString3}");
}
static void datetimeParsing()
{
	Console.Write("Masukkan tanggal: ");
	string dateString = Console.ReadLine();


	try
	{
		DateTime dt = DateTime.ParseExact(dateString, "dd/MM/yyyy", null);
		Console.WriteLine(dt);

	}
	catch (Exception ex)
	{
		Console.WriteLine("Format yang anda masukkan salah!");
		Console.WriteLine("Pesan error: " + ex.Message);
	}
}
static void datetime()
{
	DateTime dt1 = new DateTime();
	Console.WriteLine(dt1);

	DateTime dt2 = DateTime.Now;
	Console.WriteLine(dt2);

	DateTime dt3 = new DateTime(2023, 5, 4);
	Console.WriteLine(dt3);

	DateTime dt4 = new DateTime(2023, 5, 4, 11, 55, 55);
	Console.WriteLine(dt4);
}
static void indexOf()
{
	List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6 };
	int[] array = new int[] { 6, 7, 8, 9, 10 };
	Console.Write("Masukkan data item yang dicari: ");
	int item = int.Parse(Console.ReadLine());

	int indexList = list.IndexOf(item);
	int indexArray = Array.IndexOf(array, item);

	if (indexList != -1)
	{
		Console.WriteLine("List element {0} is found at index {1}", item, indexList);
	}
	else
	{
		Console.WriteLine("Element no found in the given List");
	}

	if (indexArray != -1)
	{
		Console.WriteLine("Array element {0} is found at index {1}", item, indexArray);
	}
	else
	{
		Console.WriteLine("Element no found in the given Array");
	}
}
static void stringToCharArray()
{
	Console.WriteLine("--String to char array--");
	Console.Write("Masukkan kalimat: ");
	string kal = Console.ReadLine();
	char[] kalArray = kal.ToCharArray();

	for (int i = 0; i < kalArray.Length; i++)
	{
		Console.WriteLine(kalArray[i]);
	}
}
static void Continue()
{
	for (int i = 0; i < 10; i++)
	{
		if (i == 8)
			continue;
		Console.WriteLine(i);
	}
}
static void Break()
{
	for (int i = 0; i < 10; i++)
	{
		if (i == 6)
			break;
		Console.WriteLine(i);
	}
}