﻿using Logic04;

/*MATERI OOP*/

//abstractClass();
//objectClass();
//constructor();
//encapsulation();
//inheritance();
//overriding();
//returnTypePenjumlahan();
//rekursif();
//rekursifASC();

Console.ReadKey();

static void rekursifASC()
{
	Console.WriteLine("--Rekursif Function (ASC)--");
	Console.Write("Masukkan input : ");
	int input = int.Parse(Console.ReadLine());

	int start = 1;
	PerulanganASC(input,start);
}

static int PerulanganASC(int input,int start)
{
	if (input == start)
	{
		Console.WriteLine(start);
		return start;
	}

	Console.WriteLine(start);
	return PerulanganASC(input, start + 1);
}

static void rekursif()
{
	Console.WriteLine("--Rekursif Function (DESC)--");
	Console.Write("Masukkan input: ");
	int input = int.Parse(Console.ReadLine());

	Perulangan(input);
}
static int Perulangan(int input)
{
	if(input == 1)
	{
		Console.WriteLine(input);
		return input;
	}

	Console.WriteLine(input);
	return Perulangan(input-1);
}
static void returnTypePenjumlahan()
{
	Console.WriteLine("--Method Return Type--");
	Console.Write("Jumlah mangga : ");
	int mangga = int.Parse(Console.ReadLine());
	Console.Write("Jumlah apel : ");
	int apel = int.Parse(Console.ReadLine());

	Console.WriteLine($"Total buah adalah {hitungJumlah(mangga, apel)}");
}

static int hitungJumlah(int mangga, int apel)
{
	return mangga + apel;
}
static void overriding()
{
	Console.WriteLine("--Overriding--");
	Kucing kucing = new Kucing();
	Paus paus = new Paus();

	kucing.pindah();
	paus.pindah();
}
static void inheritance()
{
	Console.WriteLine("---Inheritance---");
	TypeMobil typeMobil = new TypeMobil();
	typeMobil.Civic();
}
static void encapsulation()
{
	Console.WriteLine("---Konsep Encapsulation---");
	PersegiPanjang pp = new PersegiPanjang();
	pp.panjang = 10;
	pp.lebar = 20;
	pp.TampilkanData();

}
static void constructor()
{
	Console.WriteLine("--Constructor--");
	Mobil mobil = new Mobil("B 1234 MX");
	string platno = mobil.getPlatNo();

	Console.WriteLine($"Mobil dengan Nomor Polisi : {platno}");

}
static void objectClass()
{
	Console.WriteLine("---Object Class---");
	Mobil mobil = new Mobil("") {nama ="Ferrari", kecepatan=0, bensin=10, posisi=0 };
	//contoh panggil pakai cara lain
	//Mobil mobil2 = new Mobil();
	//mobil2.nama = "Hyundai";
	//mobil2.kecepatan = 0;
	//mobil2.bensin = 10;
	//mobil2.posisi = 0;

	mobil.percepatan();
	mobil.maju();
	mobil.isiBensin(20);

	mobil.utama();
}
static void abstractClass()
{
	Console.WriteLine("--Abstract Class--");
	Console.Write("Masukkan input x: ");
	int x = int.Parse(Console.ReadLine());
	Console.Write("Masukkan input y: ");
	int y = int.Parse(Console.ReadLine());

	TestTurunan calculator = new TestTurunan();
	int jumlah = calculator.Jumlah(x, y);
	int kurang = calculator.Kurang(x, y);

	Console.WriteLine("Hasil perjumlahan x + y = " + jumlah);
	Console.WriteLine("Hasil pengurangan x - y = " + kurang);
}