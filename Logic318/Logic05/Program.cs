﻿
namespace Logic05
{
	public class Program
	{
		//untuk baca program di Gateway
        public Program()
        {            
			Menu();
        }
        static void Main(string[] args)
		{
			Menu();
		}

		public static void Menu()
		{
			Console.WriteLine("=== Welcome to Day 05 ===");
			Console.WriteLine("|   1. Soal01        	 |");
			Console.WriteLine("|   2. Soal02            |");
			Console.WriteLine("|   3. Soal03		 |");
			Console.WriteLine("|   4. Soal04            |");
			Console.WriteLine("|   5. Soal05	         |");
			Console.WriteLine("|   6. Soal06	         |");
			Console.WriteLine("|   7. Soal07	         |");
			Console.WriteLine("|   8. Soal08	         |");
			Console.WriteLine("|   9. Soal09	         |");
			Console.WriteLine("|   10. Soal10	         |");
			Console.WriteLine("|   11. Soal Array 01	 |");
			Console.WriteLine("|   12. Array 2D Soal 1	 |");
			Console.WriteLine("|   13. Array 2D Soal 2	 |");
			Console.WriteLine("|   14. Array 2D Soal 3	 |");
			Console.WriteLine("|   15. Array 2D Soal 4	 |");
			Console.WriteLine("|   16. Array 2D Soal 5	 |");
			Console.WriteLine("|   17. Array 2D Soal 6	 |");
			Console.WriteLine("|   18. Array 2D Soal 7	 |");
			Console.WriteLine("|   19. Array 2D Soal 8	 |");
			Console.WriteLine("|   20. Array 2D Soal 9	 |");
			Console.WriteLine("|   21. Array 2D Soal 10 |");
			Console.WriteLine("|   0. Exit              |");
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					Soal01 soal01 = new Soal01();
					break;
				case 2:
					Soal02 soal02 = new Soal02();
					break;
				case 3:
					Soal03 soal03 = new Soal03();
					break;
				case 4:
					Soal04 soal04 = new Soal04();
					break;
				case 5:
					Soal05 soal05 = new Soal05();
					break;
				case 6:
					Soal06 soal06 = new Soal06();
					break;
				case 7:
					Soal07 soal07 = new Soal07();
					break;
				case 8:
					Soal08 soal08 = new Soal08();
					break;
				case 9:
					Soal09 soal09 = new Soal09();
					break;
				case 10:
					Soal10 soal10 = new Soal10();
					break;
				case 11:
					SoalArray01 soalArray01 = new SoalArray01();
					break;
				case 12:
					Array2DSoal1 array2DSoal1 = new Array2DSoal1();
					break;
				case 13:
					Array2DSoal2 array2DSoal2 = new Array2DSoal2();
					break;
				case 14:
					Array2DSoal3 array2DSoal3 = new Array2DSoal3();
					break;
				case 15:
					Array2DSoal4 array2DSoal4 = new Array2DSoal4();
					break;
				case 16:
					Array2DSoal5 array2DSoal5 = new Array2DSoal5();
					break;
				case 17:
					Array2DSoal6 array2DSoal6 = new Array2DSoal6();
					break;
				case 18:
					Array2DSoal7 array2DSoal7 = new Array2DSoal7();
					break;
				case 19:
					Array2DSoal8 array2DSoal8 = new Array2DSoal8();
					break;
				case 20:
					Array2DSoal9 array2DSoal9 = new Array2DSoal9();
					break;
				case 21:
					Array2DSoal10 array2DSoal10 = new Array2DSoal10();
					break;
				default:
					break;
			}
			Console.Write("\nPress any key...");
			Console.ReadKey();
		}
	}

}
