﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	public class Soal02
	{
		public Soal02()
		{
			//2	4	6	8	10	12	14
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine());
			string[] arrString = new string[jumlah];
			int angka = 0;
			for (int i = 0; i<jumlah; i++)
			{
				angka += 2;
				//Console.Write(angka + "\t");
				arrString[i] = angka.ToString();
			}
			Printing.Array1Dim(arrString);
		}
}
}
