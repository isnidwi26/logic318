﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	internal class Soal04
	{
		public Soal04() 
		{
			//1	 5	9	13	17	21	25
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine()); 
			string[] arrString = new string[jumlah];
			//int angka = 1;
			for (int i = 0; i < jumlah; i++)
			{
				//Console.Write((i*4+1) + "\t");
				//angka += 4;
				arrString[i] = (i * 4 + 1).ToString();
			}

			Printing.Array1Dim(arrString);
		}
	}
}
