﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	internal class Soal05
	{
		public Soal05() 
		{
			//1	 5	*	9	13	*	17
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine());
			string[] arrString = new string[jumlah];
			//int angka = 1;
			for (int i = 0; i < jumlah; i++)
			{
				//Console.Write((i * 4 + 1) + "\t");
				arrString[i] = (i * 4 + 1).ToString();
				if (i % 2 == 1)
				{
					arrString[i] = (i * 4 + 1).ToString() + "\t*";
					jumlah -= 1;
				}
			}
			Printing.Array1Dim(arrString);
		}
	}
}
