﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	internal class Soal06
	{
		public Soal06() 
		{
			//1	 5	*	13	17	*	25
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine()); ;
			//int angka = 1;
			//for (int i = 1; i <= jumlah; i++)
			//{
			//	if(i % 3 == 0)
			//		Console.Write("* \t");
			//	else
			//		Console.Write((i*4-3) + "\t");
			//	//angka += 4;
			//}
			string[] arrString = new string[jumlah];

			for(int i = 1; i <= jumlah; i++)
			{
				arrString[i - 1] = i % 3 != 0 ? $"{i * 4 - 3}" : "*";
			}
			Printing.Array1Dim(arrString);
		}
	}
}
