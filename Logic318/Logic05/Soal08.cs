﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	internal class Soal08
	{
		public Soal08() 
		{
			//3	9	27	81	243	729	2187
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine());
			string[] arrString = new string[jumlah];
			for(int i = 0; i < jumlah; i++)
			{
				//Console.Write(Math.Pow(3, i) + "\t");
				arrString[i] = Math.Pow(3, i + 1).ToString();
			}
			Printing.Array1Dim(arrString);
		}
	}
}
