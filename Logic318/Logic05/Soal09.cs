﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	internal class Soal09
	{
		public Soal09() 
		{
			//4	16	*	64	256	*	1024
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine()); ;
			int angka = 4;
			string[] arrString = new string[jumlah];
			for (int i = 1; i <= jumlah; i++)
			{
				if (i % 3 == 0)
					//Console.Write("* \t");
					arrString[i-1] = "*";
				else
				{
					//Console.Write(angka + "\t");
					arrString[i-1] = angka.ToString();
					angka *= 4;
				}
			}
			Printing.Array1Dim(arrString);
		}
	}
}
