﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class AVeryBigSum
	{
        public AVeryBigSum()
        {
            Console.Write("Masukkan deret angka: ");
            long[] deret = Array.ConvertAll(Console.ReadLine().Split(" "), long.Parse);

            long sum = 0;
            for(int i = 0; i < deret.Length; i++)
            {
                sum+= deret[i];
            }

            Console.WriteLine(sum);
        }
    }
}
