﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class BirthdayCakeCandles
	{
        public BirthdayCakeCandles()
        {
            Console.Write("Masukkan tinggi lilin: ");
            int[] lilin = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int maks = 0, banyakMaks = 0;

            for(int i = 0; i < lilin.Length; i++)
            {
                if (lilin[i] > maks)
                {
                    maks = lilin[i];
                    banyakMaks = 0;
                }
                if (lilin[i] == maks)
                    banyakMaks++;
            }
			//Console.WriteLine("\tNilai lilin tertinggi adalah: " + maks);
			Console.WriteLine("\tJumlah lilin tertinggi sebanyak: " + banyakMaks);
		}
    }
}
