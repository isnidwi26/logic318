﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class CompareTheTriplets
	{
        public CompareTheTriplets()
        {
            Console.Write("Deret angka Alice: ");
            int[] alice = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("Deret angka Bob: ");
            int[] bob = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int skorAlice = 0, skorBob = 0;
            for(int i = 0; i < alice.Length; i++)
            {
                if (alice[i] > bob[i]) 
                    skorAlice++;
                else if (bob[i] > alice[i])
                    skorBob++;
            }

            Console.WriteLine($"skor alice: {skorAlice} skorBob: {skorBob}");
        }
    }
}
