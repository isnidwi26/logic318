﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic06
{
	internal class DiagonalDifference
	{
		//public DiagonalDifference()
		//{			
		//	int[,] angka = new int[3, 3];
		//	//Perulangan input
		//	for (int i = 0; i < angka.GetLength(0); i++)
		//	{
		//		for (int j = 0; j < angka.GetLength(1); j++)
		//		{
		//			Console.Write($"Inputkan matriks [{i},{j}]: ");
		//			angka[i, j] = int.Parse(Console.ReadLine());
		//		}
		//	}

		//	int diagonal1 = 0, diagonal2 = 0;
		//	//Perulangan tampilan matriks sekaligus perhitungan diagonal difference
		//	for (int i = 0; i < angka.GetLength(0); i++)
		//	{
		//		for (int j = 0; j < angka.GetLength(1); j++)
		//		{
		//			Console.Write(angka[i, j] + "\t");
		//			if (i == j)
		//				diagonal1 += angka[i, j];
		//			if (i + j == angka.GetLength(1) - 1)
		//				diagonal2 += angka[i, j];
		//		}
		//		Console.WriteLine();
		//	}

		//	//Perulangan hitung diagonal
		//	Console.WriteLine("Diagonal 1: " + diagonal1);
		//	Console.WriteLine("Diagonal 2: " + diagonal2);
		//	Console.WriteLine($"Diagonal Difference |{diagonal1}-{diagonal2}| = {Math.Abs(diagonal1 - diagonal2)}");

		//}

		public DiagonalDifference()
		{
			Console.WriteLine();
			Console.WriteLine("=== Diagonal Difference ===");
			Console.Write("Masukkan n : ");
			int n = int.Parse(Console.ReadLine());

			int[,] matrix = new int[n, n];
			int mainDiagonal = 0, subDiagonal = 0;

			for (int i = 0; i < n; i++)
			{
				int[] arrayInput = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
				for (int j = 0; j < n; j++)
				{
					if (i == j)
					{
						mainDiagonal += arrayInput[j];
					}
					if (i + j == n - 1)
					{
						subDiagonal += arrayInput[j];
					}
					matrix[i, j] = arrayInput[j];
				}
			}
			int diagonalDiff = Math.Abs(mainDiagonal - subDiagonal);
			Printing.Array2Dim(matrix);

			Console.WriteLine($"Diagonal Difference = {diagonalDiff}");
		}
	}
}
