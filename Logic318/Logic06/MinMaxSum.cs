﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class MinMaxSum
	{
		public MinMaxSum()
		{
			Console.Write("Masukkan deret angka: ");
			int[] arrAngka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);



			//int hasilMax = 0, hasilMin = 0;
			//int min = arrAngka.Min(), max = arrAngka.Max();

			//for(int i = 0; i < arrAngka.Length; i++)
			//{
			//    hasilMax += arrAngka[i];
			//    hasilMin += arrAngka[i];
			//}

			//hasilMax -= max;
			//hasilMin -= min;

			//Console.WriteLine("Hasil max: " + hasilMax);
			//Console.WriteLine("Hasil min: " + hasilMin);

			int[] result = new int[arrAngka.Length];

			for (int i = 0; i < arrAngka.Length; i++)
			{
				for (int j = 0; j < arrAngka.Length; j++)
				{
					if (i == j)
						continue;
					result[i] += arrAngka[j];
					Console.Write(arrAngka[j] + "+");
				}
				Console.WriteLine();
			}

			Array.Sort(result);
			Console.WriteLine($"{result[0]} {result[arrAngka.Length - 1]}");
		}
	}
}
