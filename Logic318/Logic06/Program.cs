﻿namespace Logic06
{
	public class Program
	{
		public Program()
		{
			Menu();
		}

		static void Main(string[] args)
		{
			Menu();
		}

		public static void Menu()
		{

			string answer = "t";
			while (answer.ToLower() == "t")
			{
				Console.WriteLine("\n ===== Welcome to Day 06 =====");
				Console.WriteLine("|   1. Solve Me First		|");
				Console.WriteLine("|   2. Time Conversion		|");
				Console.WriteLine("|   3. Simple Array		|");
				Console.WriteLine("|   4. Diagonal Difference	|");
				Console.WriteLine("|   5. Plus Minus		|");
				Console.WriteLine("|   6. Staircase		|");
				Console.WriteLine("|   7. MinMaxSum		|");
				Console.WriteLine("|   8. Birthday Cake Candles	|");
				Console.WriteLine("|   9. A Very Big Sum	|");
				Console.WriteLine("|   10. Compare The Triplets	|");
				Console.WriteLine("|   0. Exit			|");
				Console.Write("Masukkan no soal: ");
				int soal = int.Parse(Console.ReadLine());

				switch (soal)
				{
					case 1:
						SolveMeFirst svm = new SolveMeFirst();
						break;
					case 2:
						TimeConversion tc = new TimeConversion();
						break;
					case 3:
						SimpleArraySum sam = new SimpleArraySum();
						break;
					case 4:
						DiagonalDifference df = new DiagonalDifference();
						break;
					case 5:
						PlusMinus pm = new PlusMinus();
						break;
					case 6:
						Staircase sc = new Staircase();
						break;
					case 7:
						MinMaxSum mms = new MinMaxSum();
						break;
					case 8:
						BirthdayCakeCandles bcc = new BirthdayCakeCandles();
						break;
					case 9:
						AVeryBigSum abs = new AVeryBigSum();
						break;
					case 10:
						CompareTheTriplets ctt = new CompareTheTriplets();
						break;
					default:
						break;
				}

				Console.Write("\nPress any key...");
				Console.ReadKey();
				Console.Write("Keluar Logic 6? [y/t] ");
				answer = Console.ReadLine();

			}
		}
	}
}