﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class SimpleArraySum
	{
		public SimpleArraySum()
		{
			Console.Write("Masukkan angka: ");
			int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
			int jumlah = 0;
			
			foreach(int item  in angka)
			{
				jumlah += item;
			}
			Console.Write(string.Join(" + ", angka) + " = " + jumlah);
		}
	}
}
