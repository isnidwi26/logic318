﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class SolveMeFirst
	{
        public SolveMeFirst()
        {
            Console.Write("Masukkan nilai a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b: ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine($"Penjumlahan a + b = {Addition(a, b)}");
        }

        public int Addition(int a, int b)
        {
            return a + b;
        }
    }
}
