﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	internal class Staircase
	{
		public Staircase() 
		{
			Console.Write("Masukkan panjang segitiga: ");
			int panjang = int.Parse(Console.ReadLine());

			for (int i = 0; i < panjang; i++)
			{
				Console.Write("\t\t\t");
				for (int j = 0; j < panjang; j++)
				{
					//if (j < panjang - 1 - i)
					//	Console.Write(" ");
					//else
					//	Console.Write("*");

					//kasus jika garis segitiganya cuman diluar saja
					if (i + j == panjang - 1 || j == panjang - 1 || i == panjang - 1)
						Console.Write("*");
					else
						Console.Write(" ");

				}
				Console.WriteLine();
			}
		}
	}
}
