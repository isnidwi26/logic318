﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class CaesarChiper
	{
		public CaesarChiper()
		{
			Console.Write("inputan: ");
			char[] s = Console.ReadLine().ToCharArray();
			Console.Write("jumlah rotasi: ");
			int k = int.Parse(Console.ReadLine());
			k = k % 26;

			string alfabet = "abcdefghijklmnopqrstuvwxyz";
			string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			string rotAlfabet = alfabet.Substring(k) + alfabet.Substring(0, k);
			string rotAlfabetUpper = upper_case.Substring(k) + upper_case.Substring(0, k);
	
			
			for (int i = 0; i < s.Length; i++)
			{
				if (upper_case.Contains(s[i]))
				{
					int index = upper_case.IndexOf(s[i]);
					s[i] = rotAlfabetUpper[index];
				}
				if (alfabet.Contains(s[i]))
				{
					int index = alfabet.IndexOf(s[i]);
					s[i] = rotAlfabet[index];
				}
			}

			foreach (char result in s)
			{
				Console.Write(result);
			}		
			Console.WriteLine();
		}
	}
}
