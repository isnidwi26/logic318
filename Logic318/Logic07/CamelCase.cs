﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class CamelCase
	{
        public CamelCase()
        {
            Console.Write("Masukkan kalimat: ");
            string kalimat = Console.ReadLine();
            int sum = 0;
            for(int i = 0; i < kalimat.Length; i++)
            {
                if (i == 0)
                    sum++;
                else if (char.IsUpper(kalimat[i]))
                {
                    sum++;
                    Console.Write(" ");
                }
                Console.Write(kalimat[i]);                
            }
            Console.WriteLine("\n"+sum);
        }
    }
}
