﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class Gemstones
	{
		//public Gemstones()
		//{
		//	//Console.Write("Masukkan banyak data: ");
		//	//int n = int.Parse(Console.ReadLine());
		//	Console.Write("Masukkan data 1: ");
		//	string data1 = Console.ReadLine();
		//	Console.Write("Masukkan data 2: ");
		//	string data2 = Console.ReadLine();
		//	Console.Write("Masukkan data 3: ");
		//	string data3 = Console.ReadLine();

		//	char[] tamp = new char[] { };
		//	int count = 0;

		//	for (int i = 0; i < data1.Length; i++)
		//	{
		//		for (int j = 0; j < data2.Length; j++)
		//		{
		//			for (int k = 0; k < data3.Length; k++)
		//			{
		//				if (data1[i] == data2[j] && data2[j] == data3[k])
		//				{
		//					count++;
		//					//tamp[i] = data1[i];

		//				}
		//			}
		//		}
		//	}

		//	Console.WriteLine("Jumlah Mineral: " + count);

		//}

		public Gemstones()
		{
			Console.WriteLine();
			Console.WriteLine("---Gemstones---");
			Console.Write("Input string : ");
			int n = int.Parse(Console.ReadLine());
			Console.WriteLine();

			string[] gems = new string[n];
			int gems_largest = 0;
			for (int i = 0; i < n; i++)
			{
				Console.Write($"Masukkan string ke-{i} = ");
				gems[i] = Console.ReadLine();
				if (gems_largest < gems[i].Length)
				{
					gems_largest = gems[i].Length;
				}
			}

			string[,] getgems = new string[n, gems_largest];
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < gems[i].Length; j++)
				{
					getgems[i, j] = gems[i].Substring(j, 1);
				}
			}

			//ccdda gems[i].
			//cd distinct_gems[i,k]
			string[,] distinct_gems = new string[n, gems_largest];
			int[] distinct_length = new int[n];
			int skip, limit;
			for (int i = 0; i < n; i++)
			{
				distinct_gems[i, 0] = gems[i].Substring(0, 1);
				limit = 1;
				for (int j = 0; j < gems[i].Length; j++)
				{
					skip = 0;
					for (int k = 0; k < limit; k++)
					{
						if (gems[i].Substring(j, 1) == distinct_gems[i, k])
						{
							skip++;
						}
					}
					if (skip == 0)
					{
						distinct_gems[i, limit] = gems[i].Substring(j, 1);
						limit++;
					}
				}
				distinct_length[i] = limit;
			}

			int ismatch, index=0;
			int count = 0;
			string[] gemstones = new string[gems_largest];
			int gemstone_length = 0;
			for (int i = 0; i < n - 1; i++)
			{
				index = 0;
				for (int j = 0; j < distinct_length[i]; j++)
				{
					ismatch = 0;
					for (int k = i + 1; k < n; k++)
					{
						for (int l = 0; l < distinct_length[i]; l++)
						{
							if (distinct_gems[i, j] == distinct_gems[k, l])
							{
								ismatch++;
							}
						}
					}
					if (ismatch == n - 1)
					{
						count++;
						gemstones[index] = distinct_gems[i, j];
						index++;
						gemstone_length++;
					}
				}
			}
			/*
			Console.WriteLine();
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < gems[i].Length; j++)
				{
					Console.WriteLine($"Original Gems[{i}][{j}] = {getgems[i, j]}");
				}
				Console.WriteLine();
			}
			*/
			/*
			Console.WriteLine();
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < distinct_length[i]; j++)
					{
					Console.WriteLine($"Selected Gems[{i}][{j}] = {distinct_gems[i, j]}");
				}
				Console.WriteLine();
			}
			*/


			Console.WriteLine();
			for (int i = 0; i < n; i++)
			{
				Console.Write($"Original Gems = ");
				for (int j = 0; j < gems[i].Length; j++)
				{
					Console.Write($"{getgems[i, j]} ");
				}
				Console.WriteLine();
			}

			Console.WriteLine();
			for (int i = 0; i < n; i++)
			{
				Console.Write($"Selected Gems[{i}] = ");
				for (int j = 0; j < distinct_length[i]; j++)
				{
					Console.Write($"{distinct_gems[i, j]} ");
				}
				Console.WriteLine();
			}

			/*
			Console.WriteLine();
			for (int i = 0; i < gemstone_length; i++)
			{
				Console.WriteLine($"Gemstones[{i}] = {gemstones[i]}");
			}
			Console.WriteLine();
			*/

			Console.WriteLine();
			Console.Write("Gemstones = ");
			for (int i = 0; i < gemstone_length; i++)
			{
				Console.Write($"{gemstones[i]} ");
			}
			Console.WriteLine();
			Console.WriteLine($"Amount of gemstones = {count}");
		}
	}
}
