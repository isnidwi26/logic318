﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class MakingAnagrams
	{
		public MakingAnagrams()
		{
			Console.Write("Masukkan data 1: ");
			string data1 = Console.ReadLine();
			Console.Write("Masukkan data 2: ");
			string data2 = Console.ReadLine();

			List<char> list1 = data1.ToList();
			List<char> list2 = data2.ToList();


			int sum = 0;
			for (int i = 0; i < list1.Count; i++)
			{
				if (list2.Contains(list1[i]))
				{
					sum++;
					list2.Remove(list1[i]);
				}
			}
			Console.WriteLine(data1.Length + data2.Length - (sum * 2));
		}
	}
}
