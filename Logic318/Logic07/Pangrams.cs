﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class Pangrams
	{
        public Pangrams()
        {
            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            Console.Write("Masukkan kalimat: ");
            string kalimat = Console.ReadLine().ToLower();

            bool pangram = true;
            for(int i = 0; i < alfabet.Length; i++)
            {
                if (!kalimat.Contains(alfabet[i]))
                {
                    pangram = false;
                }
            }
            Console.WriteLine(pangram ? "Pangram" : "Not pangram");

            //if (pangram)
            //    Console.Write("Pangram");
            //else
            //    Console.Write("Not Pangram");


        }
    }
}
