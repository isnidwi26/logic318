﻿namespace Logic07;

public class Program
{
    public Program()
    {
		Menu();
    }

    static void Main(string[] args)
    {
		Menu();
    }

	static void Menu()
	{
		string answer = "t";
		while (answer.ToLower() == "t")
		{
			Console.WriteLine("\n ===== Welcome to Day 07 =====");
			Console.WriteLine("|   1. Camel Case		|");
			Console.WriteLine("|   2. Strong Password		|");
			Console.WriteLine("|   3. Caesar Chiper		|");
			Console.WriteLine("|   4. Mars Exploration		|");
			Console.WriteLine("|   5. Hacker Rank in a String	|");
			Console.WriteLine("|   6. Pangrams			|");
			Console.WriteLine("|   7. Separate the Numbers	|");
			Console.WriteLine("|   8. Gemstones		|");
			Console.WriteLine("|   9. Making Anagrams		|");
			Console.WriteLine("|   10. Two Strings		|");
			Console.WriteLine("|   0. Exit			|");
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					CamelCase cc = new CamelCase();
					break;
				case 2:
					StrongPassword sp = new StrongPassword();
					break;
				case 3:
					CaesarChiper caesarChiper = new CaesarChiper();
					break;
				case 4:
					MarsExploration me = new MarsExploration();
					break;
				case 5:
					HackerRankInAString hackerRank = new HackerRankInAString();
					break;
				case 6:
					Pangrams pangrams = new Pangrams();
					break;
				case 7:
					break;
				case 8:
					Gemstones gemstones = new Gemstones();
					break;
				case 9:
					MakingAnagrams makingAnagrams = new MakingAnagrams();
					break;
				case 10:
					TwoStrings twoStrings = new TwoStrings();
					break;

				default:
					break;
			}

			Console.Write("\nPress any key...");
			Console.ReadKey();
			Console.Write("Keluar Logic 7? [y/t] ");
			answer = Console.ReadLine();
		}
	}
}
