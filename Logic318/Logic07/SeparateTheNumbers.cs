﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class SeparateTheNumbers
	{
        public SeparateTheNumbers()
        {
            Console.Write("Input n: ");
            int n = int.Parse(Console.ReadLine());
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Masukkan data: ");
                string data = Console.ReadLine();
                bool valid = false;
                long firstx = -1;
                for(int j = 1; j < data.Length/2; ++j)
                {
                    long x = Convert.ToInt64(data.Substring(0, j));
                    firstx = x;
                    string test = x.ToString();
                    while (test.Length < data.Length)
                    {
                        test += (++x).ToString();
                    }
                    if(test == data)
                    {
                        valid = true; 
                        break;
                    }
                }
                Console.Write(valid ? "Yes " + firstx : "No");
            }
        }
    }
}
