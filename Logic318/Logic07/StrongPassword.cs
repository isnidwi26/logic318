﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class StrongPassword
	{
		public StrongPassword() 
		{
			string numbers = "0123456789";
			string lower_case = "abcdefghijklmnopqrstuvwxyz";
			string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			string special_characters = "!@#$%^&*()-+";

			Console.Write("Input Password: ");
			string pass = Console.ReadLine();

			char[] password = pass.ToCharArray();

			int no = 1, lower=1, upper=1, special=1;

			for(int i =0; i < pass.Length; i++)
			{
				if (numbers.Contains(password[i]) && no == 1)
					no = 0;
				else if (lower_case.Contains(password[i]) && lower == 1)
					lower = 0;
				else if (upper_case.Contains(password[i]) && upper==1)
					upper = 0;
				else if (special_characters.Contains(password[i]) && special == 1)
					special = 0;
			}

			int result = Math.Max(6 - pass.Length, no + lower + upper + special);
			Console.WriteLine(result);

		}
	}
}
