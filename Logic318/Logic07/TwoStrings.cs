﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
	internal class TwoStrings
	{
        public TwoStrings()
        {
            Console.Write("Masukkan data 1: ")
;           string data1 = Console.ReadLine();
            Console.Write("Masukkan data 2: ");
            string data2 = Console.ReadLine();

            bool isTwoString = false;
            for(int i = 0; i< data1.Length; i++)
            {
                for(int j= 0; j<data2.Length; j++)
                {
                    if (data1[i] == data2[j])
                    {
                        isTwoString = true;
                        break;
                    }
                }
            }
            Console.WriteLine(isTwoString ? "Yes" : "No");
        }
    }
}
