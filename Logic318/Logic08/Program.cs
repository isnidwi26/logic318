﻿namespace Logic08;

public class Program
{
	public Program()
	{
		Menu();
	}

	static void Main(string[] args)
	{
		Menu();
	}

	static void Menu()
	{
		string answer = "t";
		while (answer.ToLower() == "t")
		{
			Console.WriteLine("\n ===== Welcome to Day 08 =====");
			Console.WriteLine("|   1. Find The Median		|");
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					FindTheMedian sort = new FindTheMedian();
					break;

				default:
					break;
			}

			//int[] randomArr = new int[] {5, 4, 3, 2, 1};
			//Utility.Printing.Array1Dim(Array.ConvertAll(randomArr, x => x.ToString()));
			//Console.WriteLine();
			//int[] sortedArr = Utility.Sorting.InsertionSort(randomArr);
			//Utility.Printing.Array1Dim(Array.ConvertAll(sortedArr, x => x.ToString()));

			Console.Write("\nPress any key...");
			Console.ReadKey();
			Console.Write("Keluar Logic 8? [y/t] ");
			answer = Console.ReadLine();
		}
	}
}
