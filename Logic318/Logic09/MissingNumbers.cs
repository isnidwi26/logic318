﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
	internal class MissingNumbers
	{
        public MissingNumbers()
        {

			//203 204 205 206 207 208 203 204 205 206
			//203 204 204 205 206 207 205 208 203 206 205 206 204
			Console.WriteLine("Masukkan data1: ");
            int[] data1 = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.WriteLine("Masukkan data2: ");            
            int[] data2 = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            List<int> list1 = data1.ToList();
            list1.Sort();
            List<int> list2 = data2.ToList();
            list2.Sort();
            List<int> list3 = new List<int>();


            //for (int i = 0; i < list1.Count; i++)
            //{
            //    if (!list2.Contains(list1[i]))
            //        list3.Add(list1[i]);
            //    if (list2.Contains(list1[i]))
            //        list2.Remove(list1[i]);
            //}

            for (int i=0; i < list2.Count; i++)
            {
                if (!list1.Contains(list2[i]))
                    list3.Add( list2[i]);
                if (list1.Contains(list2[i]))
                    list1.Remove(list2[i]);
            }



            Console.WriteLine();

			for (int i=0; i<list3.Count; i++)
            {
                Console.WriteLine(list3[i]);
            }
        }
    }
}
