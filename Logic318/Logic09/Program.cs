﻿namespace Logic09;

public class Program
{
	public Program()
	{
		Menu();
	}

	static void Main(string[] args)
	{
		Menu();
	}

	static void Menu()
	{
		string answer = "t";
		while (answer.ToLower() == "t")
		{
			Console.WriteLine("\n ===== Welcome to Day 09 =====");
			Console.WriteLine("|   1. Missing Numbers		|");
			Console.WriteLine("|   2. Sherlock and Array	|");
			Console.WriteLine("|   3. Date Time Review		|");
			Console.WriteLine("|   4. Ice Cream Parlor		|");
			Console.WriteLine("|   5. Pairs			|");
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					MissingNumbers missingNumbers = new MissingNumbers();
					break;
				case 2:
					SherlockAndArray sherlockAndArray = new SherlockAndArray();
					break;
				case 3:
					DateTimeReview dateTimeReview = new DateTimeReview();
					break;
				case 4:
					IceCreamParlor iceCreamParlor = new IceCreamParlor();
					break;
				case 5:
					Pairs pairs = new Pairs();
					break;
				default:
					break;
			}

			Console.Write("\nPress any key...");
			Console.ReadKey();
			Console.Write("Keluar Logic 9? [y/t] ");
			answer = Console.ReadLine();
		}
	}
}
