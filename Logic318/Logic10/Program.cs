﻿namespace Logic10;

public class Program
{
	public Program()
	{
		Menu();
	}

	static void Main(string[] args)
	{
		Menu();
	}

	static void Menu()
	{
		string answer = "t";
		while (answer.ToLower() == "t")
		{
			Console.WriteLine("\n ===== Welcome to Day 10 =====");
			Console.WriteLine("|   1. Simplified Chess Engine	|");
			Console.WriteLine("|   2. Bilangan Prima			|");
			Console.WriteLine("|   3. Progrresive Tax			|");
			Console.WriteLine("|   4. Gross Up Tax			|");
			
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					SimplifiedChessEngine simplifiedChessEngine = new SimplifiedChessEngine();
					break;
				case 2:
					BilPrima bilPrima = new BilPrima();
					break;
				case 3:
					ProgressiveTax progressiveTax = new ProgressiveTax();
					break;
				case 4:
					GrossUpTax grossUpTax = new GrossUpTax();
					break;
				default:
					break;
			}

			Console.Write("\nPress any key...");
			Console.ReadKey();
			Console.Write("Keluar Logic 10? [y/t] ");
			answer = Console.ReadLine();
		}
	}
}
