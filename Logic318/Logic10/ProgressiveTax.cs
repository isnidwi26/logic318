﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
	internal class ProgressiveTax
	{
		public ProgressiveTax()
		{
			Console.WriteLine("Masukkan pemasukan tahunan : ");
			double penghasilan = int.Parse(Console.ReadLine());
			Console.WriteLine(TaxAllowance(penghasilan, 0));
		}


		public static double TaxAllowance(double penghasilan, double pajak)
		{
			double pkp = penghasilan + pajak - 25000;
			double penghasilanBersih = penghasilan;
			double hitungPajak = 0;
			if (pkp < 1)
				return 0;
			else
			{
				if (pkp - 50000 > 0)
					hitungPajak += 50000 * 0.05;
				else
					hitungPajak += pkp * 0.05;

				if (pkp > 50000)
				{
					if (pkp - 50000 > 50000)
						hitungPajak += 50000 * 0.1;
					else
						hitungPajak += (pkp - 50000) * 0.1;
				}
				if (pkp > 100000)
				{
					if (pkp - 100000 > 100000)
						hitungPajak += 100000 * 0.15;
					else
						hitungPajak += (pkp - 100000) * 0.15;
				}
				if (pkp > 200000)
					hitungPajak += (pkp - 200000) * 0.25;
			}
			double tunjangan = Math.Round(hitungPajak, MidpointRounding.ToNegativeInfinity);
			if (pajak == tunjangan)
				return tunjangan;

			return TaxAllowance(penghasilanBersih, tunjangan);
		}
	}
}
