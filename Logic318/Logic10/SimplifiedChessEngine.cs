﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic10
{
	internal class SimplifiedChessEngine
	{
		public SimplifiedChessEngine()
		{
			Console.WriteLine("=== Simplified Chess Engine ===");
			Console.Write("Masukkan posisi Queen White: ");
			string qWhite = Console.ReadLine();
			Console.Write("Masukkan posisi Queen Black: ");
			string qBlack = Console.ReadLine();

			string[,] cBoard = new string[4, 4];

			string rowMap = "4321";
			string colMap = "ABCD";
			bool win = false;
			for (int i = 0; i < cBoard.GetLength(0); i++)
			{
				for (int j = 0; j < cBoard.GetLength(1); j++)
				{
					if (i == rowMap.IndexOf(qWhite[1]) && j == colMap.IndexOf(qWhite[0]))
						cBoard[i, j] = "QW";
					else if (j == colMap.IndexOf(qWhite[0]) || i == rowMap.IndexOf(qWhite[1]) ||
						rowMap.IndexOf(qWhite[1]) + j == colMap.IndexOf(qWhite[0]) + i ||
						rowMap.IndexOf(qWhite[1]) + colMap.IndexOf(qWhite[0]) == i + j)
						cBoard[i, j] = "*";
					else
						cBoard[i, j] = ".";



				}
				Console.WriteLine();
			}
			if (cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] == "*")
				win = true;
			

			Printing.Array2Dim(cBoard);
			Console.WriteLine(win ? "Yes" : "No");

		}
	}
}
