﻿
//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();




static void soal1()
{
	Console.WriteLine("-- Menghitung Keliling dan Luas Lingkaran --");
	Console.Write("Masukkan panjang jari-jari: ");
	double r = double.Parse(Console.ReadLine());
	double k, l;
	k = 2  * r * 22/7;
	l = (Math.Pow(r,2)) * 22/7;
	Console.WriteLine($"Keliling yang terhitung adalah {k} cm");
	Console.WriteLine($"Luas yang terhitung adalah {l} cm\xB2");
}

static void soal2()
{
	Console.WriteLine("-- Menghitung Keliling dan luas Persegi --");
	Console.Write("Masukkan panjang sisi: ");
	double s = double.Parse(Console.ReadLine());
	double l, k;
	l = s * s;
	k = 4 * s;
	Console.WriteLine($"Keliling Persegi yang terhitung adalah {k} cm");
	Console.WriteLine($"Luas Persegi yang terhitung adalah {l} cm");
}

static void soal3()
{
	Console.WriteLine("-- Cek Modulus --");
	Console.Write("Masukkan angka: ");
	int angka = int.Parse(Console.ReadLine());
	Console.Write("Masukkan angka pembagi: ");
	int pembagi = int.Parse(Console.ReadLine());

	if( angka % pembagi == 0)
		Console.WriteLine($"Angka {angka} % {pembagi} adalah {angka % pembagi}");
	else
		Console.WriteLine($"Angka {angka} % {pembagi} bukan 0 melainkan {angka % pembagi}");
}

static void soal4()
{
	Console.WriteLine("-- Puntung Rokok --");
	Console.Write("Masukkan jumlah puntung rokok yang telah terkumpul: ");
	int rokok = int.Parse(Console.ReadLine());

	int harga = 0, sisa = 0, jmlhRokok=0;
	Console.WriteLine($"Pemulung berhasil merangkai batang rokok menjadi {rokok / 8} batang rokok,\n" +
		$"dengan sisa puntung rokok sebanyak {rokok % 8}. \n" +
		$"Sehingga laku dijual seharga Rp.{(rokok / 8) * 500}");

}

static void soal5()
{
	Console.WriteLine("-- Grade Nilai --");
	Console.Write("Masukkan Nilai: ");
	int nilai = int.Parse(Console.ReadLine());

	if (nilai >= 80 && nilai <= 100)
		Console.WriteLine($"Nilai {nilai} mendapat grade A");
	else if (nilai >= 60 && nilai < 80)
		Console.WriteLine($"Nilai {nilai} mendapat grade B");
	else if (nilai >= 1 && nilai < 60)
		Console.WriteLine($"Nilai {nilai} mendapat grade C");
	else
		Console.WriteLine("Nilai yang anda masukkan salah!");
}

static void soal6()
{
	Console.WriteLine("-- Cek angka ganjil genap --");
	Console.Write("Masukkan angka yang akan di cek: ");
	int angka = int.Parse(Console.ReadLine());

	if (angka % 2 == 0)
		Console.WriteLine($"Angka {angka} adalah genap");
	else
		Console.WriteLine($"Angka {angka} adalah ganjil");
}