﻿
/*menu();
static void menu()
{
	int pilihMenu;
	Console.WriteLine("----------------------------");
	Console.WriteLine("1. Soal 1");
	Console.WriteLine("2. Soal 2");
	Console.WriteLine("3. Soal 3");
	Console.WriteLine("4. Soal 4");
	Console.WriteLine("5. Soal 5");
	Console.WriteLine("6. Soal 6");
	Console.WriteLine("7. Soal 7");
	Console.WriteLine("8. Soal 8");
	Console.WriteLine("----------------------------");
	Console.WriteLine("Pilih Menu	: ");
	pilihMenu = int.Parse(Console.ReadLine());
	switch (pilihMenu)
	{
		case 1: soal1(); break;
		case 2: soal2(); break;
		case 3: soal3(); break;
		case 4: soal4(); break;
		case 5: soal5(); break;
		case 6: soal6(); break;
		case 7: soal7(); break;
		case 8: soal8(); break;
		default:
			Console.WriteLine()
			break;
	}

}*/




static void soal1()
{
	Console.WriteLine("-----Grade Nilai Mahasiswa-----");
	Console.Write("Masukkan nilai: ");
	string input = Console.ReadLine().Replace(" ", "");

	if (input == "")
		Console.WriteLine("Harap input yang benar!");
	else
	{
		int nilai = int.Parse(input);

		if (nilai >= 90 && nilai <= 100)
			Console.WriteLine($"Mahasiswa dengan nilai {nilai} mendapat A");
		else if (nilai >= 70 && nilai <= 89)
			Console.WriteLine($"Mahasiswa dengan nilai {nilai} mendapat B");
		else if (nilai >= 50 && nilai <= 69)
			Console.WriteLine($"Mahasiswa dengan nilai {nilai} mendapat C");
		else if (nilai >= 1 && nilai < 50)
			Console.WriteLine($"Mahasiswa dengan nilai {nilai} mendapat E");
		else
			Console.WriteLine("Nilai yag anda masukkan salah!");
	}

}

static void soal2()
{
	Console.WriteLine("-----Poin Pulsa-----");
	int poin = 0, pulsa = 0;

	try
	{
		while (true)
		{
			Console.Write("Pulsa: ");
			pulsa = int.Parse(Console.ReadLine());
			if (pulsa < 0)
				Console.WriteLine("Jumlah pulsa yang anda masukkan salah");
			else
				break;
		}


		if (pulsa >= 10000 && pulsa < 25000)
			poin = 80;
		else if (pulsa >= 25000 && pulsa < 50000)
			poin = 200;
		else if (pulsa >= 50000 && pulsa < 100000)
			poin = 400;
		else if (pulsa >= 100000)
			poin = 800;
		else if (pulsa < 10000)
		{
			poin = 0;
		}
		Console.WriteLine("---------------------");
		Console.WriteLine($"Pulsa	:{pulsa} ");
		Console.WriteLine($"Point	:{poin}");
	}
	catch (Exception)
	{
		Console.WriteLine("Invalid");
	}
}

//soal3();
static void soal3()
{
ulang:
	Console.WriteLine("-----Promo GrabFood-----");
	Console.Write("Belanja : ");
	int belanja = int.Parse(Console.ReadLine());
	Console.Write("Jarak : ");
	double jarak = double.Parse(Console.ReadLine().Replace('.',','));
	Console.Write("Masukkan Promo: ");
	string promo = Console.ReadLine().ToLower();

	double potharga = 0, totalBelanja = 0, ongkir = 0;

	if (promo == "jktovo" && belanja >= 30000)
	{
		potharga = belanja * 0.4;
		if (potharga > 30000)
		{
			potharga = 300000;
			if (jarak <= 5 && jarak >= 1)
				ongkir = 5000;
			else if (jarak > 5)
				ongkir = jarak * 1000;
			else
				Console.WriteLine("jaraknya kurang.");
		}
		else
		{
			if (jarak <= 5 && jarak >= 1)
				ongkir = 5000;
			else if (jarak > 5)
				ongkir = jarak * 1000;
			else
				Console.WriteLine("jaraknya kurang.");
		}
	}
	else
	{
		if (jarak <= 5 && jarak >= 1)
			ongkir = 5000;
		else if (jarak > 5)
			ongkir = jarak * 1000;
		else
			Console.WriteLine("jaraknya kurang.");
	}

	totalBelanja = belanja + ongkir - potharga;
	Console.WriteLine("--------------------------");
	Console.WriteLine("Belanja			: " + belanja);
	Console.WriteLine("Diskon			: " + potharga);
	Console.WriteLine("Ongkir			: " + ongkir);
	Console.WriteLine("Total Belanja		: " + totalBelanja);
}

soal4();
static void soal4()
{
	ulang:
	Console.WriteLine("-----Marketplace Sopi-----");
	Console.Write("Belanja		: ");
	int belanja = int.Parse(Console.ReadLine());
	Console.Write("Ongkos kirim	: ");
	int ongkir = int.Parse(Console.ReadLine());
	Console.Write("Pilih Vocher	: ");
	int vocher = int.Parse(Console.ReadLine());


	int diskonOngkir = 0, diskonBelanja = 0;
	if (vocher == 1 && belanja >= 30000)
	{
		diskonOngkir = 5000;
		diskonBelanja = 5000;
	}
	else if (vocher == 2 && belanja >= 50000)
	{
		diskonOngkir = 10000;
		diskonBelanja = 10000;
	}
	else if (vocher == 3 && belanja >= 100000)
	{
		diskonOngkir = 20000;
		diskonBelanja = 10000;
	}
	else
	{
		Console.WriteLine("Vocher tidak tersedia");
		Console.Write("Apakah anda ingin melanjutkan pembayaran tanpa penggunaan vocher (y/n)? ");
		string ulangi = Console.ReadLine();

		if (ulangi == "n")
		{
			Console.Clear();
			goto ulang;
		}
	}

	int totalBelanja = belanja + ongkir - diskonOngkir - diskonBelanja;
	Console.WriteLine("---------------------------");
	Console.WriteLine("Belanja		: " + belanja);
	Console.WriteLine("Ongkos kirim	: " + ongkir);
	Console.WriteLine("Diskon ongkir	: " + diskonOngkir);
	Console.WriteLine("Diskon belanja	: " + diskonBelanja);
	Console.WriteLine("Total belanja	: " + totalBelanja);
}

static void soal5()
{
	Console.WriteLine("-----Generasi Kelahiran-----");
	Console.Write("Masukkan nama anda: ");
	string nama = Console.ReadLine();
	Console.Write("Tahun berapa anda lahir: ");
	int tahun = int.Parse(Console.ReadLine());


	if (tahun >= 1944 && tahun <= 1964)
		Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Baby boomer");
	else if (tahun >= 1965 && tahun <= 1979)
		Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
	else if (tahun >= 1980 && tahun <= 1994)
		Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y");
	else if (tahun >= 1995 && tahun <= 2015)
		Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
	else
		Console.WriteLine($"{nama}, berdasarkan tahun lahir yang diinputkan tidak tergolong pada tahun generasi apapaun.");
}

static void soal6()
{
	Console.WriteLine("-----Potongan Pajak-----");
	Console.Write("Nama: ");
	string nama = Console.ReadLine();
	Console.Write("Tunjangan: ");
	double tunjangan = double.Parse(Console.ReadLine());
	Console.Write("Gapok: ");
	double gapok = double.Parse(Console.ReadLine());
	Console.Write("Banyak bulan: ");
	int bnykBulan = int.Parse(Console.ReadLine());

	double pajak = 0, bpjs = 0, gaji = 0, totalGaji = 0, gt = 0;
	gt = gapok + tunjangan;
	bpjs = 0.03 * gt;


	if (gt <= 5000000 && gt >= 0)
	{
		pajak = 0.05 * gt;
		gaji = gt - (pajak + bpjs);
		totalGaji = gt - (pajak + bpjs) * bnykBulan;
	}
	else if (gt <= 10000000 && gt > 5000000)
	{
		pajak = 0.1 * gt;
		gaji = gt - (pajak + bpjs);
		totalGaji = gt - (pajak + bpjs) * bnykBulan;
	}
	else if (gt > 10000000)
	{
		pajak = 0.15 * gt;
		gaji = gt - (pajak + bpjs);
		totalGaji = gt - (pajak + bpjs) * bnykBulan;
	}

	Console.WriteLine($"Karyawan atas nama {nama} slip haji sebagai berikut:");
	Console.WriteLine("Pajak		: " + pajak.ToString("Rp #,##0"));
	Console.WriteLine("Bpjs			: " + bpjs.ToString("Rp #,##0"));
	Console.WriteLine("Gaji/Bulan	: " + gaji.ToString("Rp #,##0"));
	Console.WriteLine("Total gaji	: " + totalGaji.ToString("Rp #,##0"));
}

static void soal7()
{
	Console.WriteLine("-----BMI-----");
	Console.Write("Masukkan berat badan anda (kg): ");
	double berat = double.Parse(Console.ReadLine());
	Console.Write("Masukkan tinggi badan anda (cm): ");
	double tinggi = double.Parse(Console.ReadLine());
	tinggi /= 100;
	double bmi = 0;
	bmi = berat / Math.Pow(tinggi, 2);
	if (bmi < 18.5)
		Console.WriteLine($"Nilai BMI anda adalah {Math.Round(bmi, 4)},\nAnda terlalu kurus");
	else if (bmi >= 18.5 && bmi < 25)
		Console.WriteLine($"Nilai BMI anda adalah {Math.Round(bmi, 4)},\nAnda termasuk berbadan langsing");
	else
		Console.WriteLine($"Nilai BMI anda adalah {Math.Round(bmi, 4)},\nAnda termasuk berbadan gemuk");
}

static void soal8()
{
	Console.Write("Masukkan nilai MTK : ");
	int mtk = int.Parse(Console.ReadLine());
	Console.Write("Masukkan nilai Fisika: ");
	int fisika = int.Parse(Console.ReadLine());
	Console.Write("Masukkan nilai kimia: ");
	int kimia = int.Parse(Console.ReadLine());
	double avg = 0;
	avg = (mtk + fisika + kimia) / 3;
	Console.WriteLine("Nilai rata-rata: " + avg);

	if (avg >= 50)
	{
		Console.WriteLine("Selamat\nKamu berhasil\nKamu hebat");
	}
	else
	{
		Console.WriteLine("Maaf\nKamu gagal");

	}
}

