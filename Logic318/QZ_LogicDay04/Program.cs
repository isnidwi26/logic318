﻿
//Upah();
static void Upah()
{
	ulang:
	Console.WriteLine("---Gaji Karyawan---");
	Console.Write("Golongan: ");
	int golongan = int.Parse(Console.ReadLine());
	Console.Write("Jam Kerja: ");
	double jamKerja = int.Parse(Console.ReadLine());

	double upah = 0, lembur = 0, total = 0;
	switch (golongan)
	{
		case 1:
			upah = 40 * 2000;
			lembur = (jamKerja - 40) * 3000;
			total = upah + lembur;
			break;
		case 2:
			upah = 40 * 3000;
			lembur = (jamKerja - 40) * 4500;
			total = upah + lembur;
			break;
		case 3:
			upah = 40 * 4000;
			lembur = (jamKerja - 40) * 6000;
			total = upah + lembur;
			break;
		case 4:
			upah = 40 * 5000;
			lembur = (jamKerja - 40) * 7500;
			total = upah + lembur;
			break;
		default:
			Console.Write("Tidak terdapat golongan tersebut, apakah anda ingin mengulang? (y/n)? ");
			string pilih = Console.ReadLine().ToLower();
			if (pilih == "y")
			{
				Console.Clear();
				goto ulang;
			}
			break;
	}

	Console.WriteLine($"Upah	: {upah,10}");
	Console.WriteLine($"Lembur	: {lembur,10}");
	Console.WriteLine($"Total	: {total,10}");
}

//pemisahKata();
static void pemisahKata()
{
	Console.WriteLine("----Pemisah kata pada kalimat----");
	Console.Write("Masukkan kalimat : ");
	string kal = Console.ReadLine();

	string[] kata = kal.Split(' ');

	for (int i = 0; i < kata.Length; i++)
	{
		Console.WriteLine($"kata ke-{i+1}: {kata[i]}");
	}
	Console.WriteLine($"Total kata adalah {kata.Length}");
}

//bintangTengah();
static void bintangTengah()
{
	Console.WriteLine("-----Menampilkan bintang pada di setiap tengah kata-----");
	Console.Write("Masukkan kalimat: ");
	string kal = Console.ReadLine();

	string[] kata = kal.Split(' ');

	for(int i=0; i < kata.Length; i++)
	{
		for(int j=0; j < kata[i].Length; j++)
		{
			if (j == 0 || j == kata[i].Length - 1)
				Console.Write(kata[i][j]);
			else
				Console.Write("*");
		}
		Console.Write(' ');
	}
}


//bintangUjung();
static void bintangUjung()
{
	Console.WriteLine("-----Menampilkan bintang pada di setiap luar ujung huruf pada kata-----");
	Console.Write("Masukkan kalimat: ");
	string kal = Console.ReadLine();

	string[] kata = kal.Split(' ');

	for (int i = 0; i < kata.Length; i++)
	{
		for (int j = 0; j < kata[i].Length; j++)
		{
			if (j == 0 || j == kata[i].Length - 1)
				Console.Write("*");
			else
				Console.Write(kata[i][j]);
		}
		Console.Write(' ');
	}
}

//hapusHuruf();
static void hapusHuruf()
{
	Console.WriteLine("-----Menghapus huruf pada awal kata-----");
	Console.Write("Masukkan kalimat: ");
	string kal = Console.ReadLine();
	string[] kata = kal.Split(' ');

	for(int i = 0;i < kata.Length; i++)
	{
		for(int j=0; j < kata[i].Length; j++)
		{
			if (j == 0)
				continue;
			Console.Write(kata[i][j]);
		}
		Console.Write(" ");
	}
}

//kelipatanTiga();
static void kelipatanTiga()
{
	Console.WriteLine("----Memberikan bintang pada kelipatan secara bergantian----");
	Console.Write("Masukkan jumlah panjang kelipatan 3 yang ditampilkan: ");
	int n = int.Parse(Console.ReadLine());

	int kelipatan = 3;

	for(int i=0 ;i < n; i++)
	{
		if (i % 2 == 1)
			Console.Write("* ");
		else
			Console.Write(kelipatan + " ");
		kelipatan*= 3;
	}
}

fibonaci();
static void fibonaci()
{
	Console.WriteLine("-----Fibonaci 2-----");
	Console.Write("Masukkan banyak yang ditampilkan: ");
	int n = int.Parse(Console.ReadLine());

	int[] fib = new int[n];

	for(int i=0 ;i < n; i++)
	{
		if (i <= 1)
			fib[i] = 1;
		else
			fib[i] = fib[i-1] + fib[i-2];
	}
	Console.Write(string.Join(',', fib));
}

//persegiBerbintang();
static void persegiBerbintang()
{
	Console.WriteLine("-----Persegi berbintang-----");
	Console.Write("Masukkan panjang persegi: ");
	int n = int.Parse(Console.ReadLine());

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			if (i == 1)
				Console.Write(j);
			else if (i == n)
				Console.Write(n-j+1);
			else if (j == 1 || j == n)
				Console.Write("*");
			else
				Console.Write(" ");
		}
		Console.WriteLine();
	}
}
