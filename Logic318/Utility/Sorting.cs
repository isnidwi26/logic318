﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Utility
{
	public static class Sorting
	{
		public static int[] InsertionSort(int[] sort)
		{
			int tamp = 0;
			for (int i = 0; i < sort.Length; i++)
			{
				for (int j = i; j > 0; j--)
				{
					if (sort[j] < sort[j-1])
					{
						tamp = sort[j-1];
						sort[j-1] = sort[j];
						sort[j] = tamp;
					}
				}
			}

			return sort;
		}

		public static void printResult(int[] data)
		{
			Console.WriteLine(string.Join(" ", data));
		}
	}
}
