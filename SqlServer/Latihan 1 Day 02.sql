CREATE DATABASE DBPenerbit

CREATE TABLE tblPengarang 
(
ID INT IDENTITY(1,1) NOT NULL,
Kd_Pengarang  VARCHAR(7) PRIMARY KEY NOT NULL,
Nama VARCHAR(30) NOT NULL,
Alamat VARCHAR(80) NOT NULL,
Kota VARCHAR(15) NOT NULL,
Kelamin VARCHAR(1) NOT NULL
)


INSERT INTO tblPengarang
VALUES
('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','P'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl. Singa 7','Bandung','P'),
('P0008','Saman','Jl. Naga 12','Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')




--No 2
CREATE VIEW vwPengarang
AS 
SELECT Kd_Pengarang, Nama, Kota
FROM tblPengarang 

DROP VIEW vwPengarang

--No a
SELECT Kd_Pengarang, Nama
FROM tblPengarang
ORDER BY Nama

--No b
SELECT Kota, Kd_Pengarang, Nama
FROM tblPengarang
ORDER BY Kota

--No c
SELECT COUNT(Kd_Pengarang)[JumlahPengarang]
FROM tblPengarang 

--No d
SELECT Kota, COUNT(Kota)[JumlahKota]
FROM tblPengarang
GROUP BY Kota

--No e
SELECT Kota, COUNT(Kota)[JumlahKota]
FROM tblPengarang
group by Kota
HAVING COUNT(Kota) > 1

--No f 
SELECT MIN(Kd_Pengarang)[PengarangTerkecil], MAX(Kd_Pengarang)[PengarangTerbesar]
FROM tblPengarang



CREATE TABLE tblGaji
(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
Kd_Pengarang VARCHAR(7) NOT NULL,
Nama VARCHAR(30) NOT NULL,
Gaji DECIMAL(18,4) NOT NULL
)

INSERT INTO tblGaji
VALUES
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwadi',1000000),
('P0010', 'Fatmawati',600000),
('P0008','Saman', 750000)


--No a
SELECT MAX(Gaji)[Tertinggi], MIN(Gaji)[Terendah]
FROM tblGaji


--No b
SELECT Gaji
FROM tblGaji
WHERE Gaji > 600000

--No c
SELECT SUM(Gaji) TotalGaji
FROM tblGaji

--No d
SELECT Kota, Gaji
FROM tblGaji
RIGHT JOIN tblPengarang p on p.Kd_Pengarang = tblGaji.Kd_Pengarang
ORDER BY Kota

--No e
SELECT *
FROM tblPengarang
WHERE Kd_Pengarang BETWEEN 'P0001' AND 'P0006'

--No f
SELECT *
FROM tblPengarang
WHERE Kota = 'Yogya' OR Kota='Solo' OR Kota='Magelang'

--No g
SELECT *
FROM tblPengarang
WHERE Kota != 'Yogya'

--No h
	-- Bagian a
		SELECT *
		FROM tblPengarang
		WHERE Nama LIKE 'A%'
	--Bagian b
		SELECT *
		FROM tblPengarang
		WHERE Nama LIKE '%i'
	--Bagian c
		SELECT *
		FROM tblPengarang
		WHERE Nama LIKE '__a%'
	--Bagian d
		SELECT *
		FROM tblPengarang
		WHERE NOT Nama LIKE '%n'
--No i
SELECT tblPengarang.Kd_Pengarang, tblPengarang.Nama, Alamat, Kota, Kelamin, Gaji
FROM tblPengarang
JOIN tblGaji ON tblPengarang.Kd_Pengarang = tblGaji.Kd_Pengarang

--No j
SELECT DISTINCT Kota
FROM tblGaji
JOIN tblPengarang ON tblGaji.Kd_Pengarang = tblPengarang.Kd_Pengarang
WHERE Gaji < 1000000

--No k
ALTER TABLE tblPengarang ALTER COLUMN Kelamin VARCHAR(10)

--No l
ALTER TABLE tblPengarang ADD Gelar VARCHAR(12)

--No m
UPDATE tblPengarang SET Alamat = 'Jl. Cendrawasih 65', Kota = 'Pekanbaru' WHERE Nama = 'Rian'






