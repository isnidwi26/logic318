create database DbMahasiswa318
 use DbMahasiswa318

 insert into Jurusan (Kode_Jurusan, Nama_Jurusan, Status_Jurusan)
 values 
 ('J001', 'Teknik Informatika', 'Aktif'),
 ('J002', 'Management Informatika', 'Aktif'),
 ('J003', 'Sistem Informasi', 'Non Aktif'),
 ('J004', 'Sistem Komputer', 'Aktif'),
 ('J005', 'Komputer Akutansi', 'Non Aktif')

 select * from Jurusan

 create table Agama
 (
 Id int   identity(1,1) not null,
 Kode_Agama char(5) primary key not null,
 Deskripsi varchar(20) not null
 )

 drop table Agama

 insert into Agama(Kode_Agama, Deskripsi)
 values
 ('A001', 'Islam'),
 ('A002', 'Kristen'),
 ('A003', 'Katolik'),
 ('A004', 'Hindu'),
 ('A005', 'Budha')

 create table Mahasiswa
 (
 Id int identity(1,1) not null,
 Kode_Mahasiswa char(5) primary key not null,
 Nama_Mahasiswa varchar(100) not null,
 Alamat varchar(200) not null,
 Kode_Agama char(5) not null,
 Kode_Jurusan char(5) not null
 )

 drop table Mahasiswa


 create table Ujian
 (
 Id int identity(1,1) not null,
 Kode_Ujian char(5) primary key not null,
 Nama_Ujian varchar(50) not null,
 Status_Ujian varchar(100) not null
 )
 drop table Ujian


 create table Type_Dosen
 (
 Id int identity(1,1) not null,
 Kode_TypeDosen char(5) primary key not null,
 Deskripsi varchar(20) not null
 )

 create table Dosen
 (
 Id int identity(1,1) not null,
 Kode_Dosen char(5) primary key not null,
 Nama_Dosen varchar(100) not null,
 Kode_Jurusan char(5) not null,
 Kode_Type_Dosen char(5) not null
 )

 create table Nilai
 (
 Id int primary key identity(1,1) not null,
 Kode_mahasiswa char(5)  not null,
 Kode_Ujian char(5) not null,
 Nilai decimal(8,2) not null
 )

 drop table Nilai



 insert into Ujian 
 values
 ('U001', 'Algoritma', 'Aktif'),
 ('U002', 'Aljabar', 'Aktif'),
 ('U003', 'Statistika', 'Non Aktif'),
 ('U004', 'Etika Profesi', 'Non Aktif'),
 ('U005', 'Bahasa Inggris', 'Aktif')


 insert into Type_Dosen
 values
 ('T001','Tetap'),
 ('T002', 'Honorer'),
 ('T003', 'Expertise')

 insert into Dosen
 values
 ('D001', 'Prof. Dr.Retno Wahyuningsih', 'J001', 'T002'),
 ('D002', 'Prof. Roy M. Sutikno', 'J002', 'T001'),
 ('D003', 'Prof. Hendri A. Verburgh', 'J003', 'T002'),
 ('D004', 'Prof. Risma Suparwata', 'J004', 'T002'),
 ('D005', 'Prof. Amir Sjarifuddin Madjid, MM, MA', 'J005', 'T001')

 insert into Nilai
 values
 ('M004','U001',90),
 ('M001','U001',80),
 ('M002','U003',85),
 ('M004','U002',95),
 ('M005','U005',70)

 insert into Mahasiswa (Kode_Mahasiswa, Nama_Mahasiswa, Alamat, Kode_Agama, Kode_Jurusan)
 values
 ('M001','Budi Gunawan','Jl. Mawar No 3 RT 05 Cicalengka, Bandung','A001','J001'),
 ('M002','Rinto Raharjo','Jl. Kebagusan No. 33 RT04 RW06 Bandung','A002','J002'),
 ('M003','Asep Saepudin','Jl. Sumatera No. 12 RT02 RW01, Ciamis','A001','J003'),
 ('M004','M. Hafif Isbullah','Jl. Jawa No 01 RT01 RW01, Jakarta Pusat','A001','J001'),
 ('M005','Cahyono','Jl. Niagara No. 54 RT01 RW09, Surabaya','A003','J002')


 
 --no 2
 alter table Dosen alter column Nama_Dosen varchar(200) not null

 --no 3
 select Kode_Mahasiswa, Nama_Mahasiswa, j.Nama_Jurusan [Nama_Jurusan], a.Deskripsi [Agama] 
 from Mahasiswa as mhs
 join Jurusan as j on j.Kode_Jurusan = mhs.Kode_Jurusan
 join Agama as a on a.Kode_Agama = mhs.Kode_Agama
 where Kode_Mahasiswa = 'M001'

 --no 4
 select *
 from Mahasiswa as mhs
 join Jurusan as jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
 where Jur.Status_Jurusan = 'Non Aktif'

 --no 5
 select *
 from Mahasiswa as mhs
 join Nilai nilai on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
 join Ujian ujian on ujian.Kode_Ujian = nilai.Kode_Ujian
 where nilai.Nilai > 80 and ujian.Status_Ujian = 'Aktif'

 --no 6
 select *
 from Jurusan jur
 where Nama_Jurusan like 'sistem%'


 --no 7
 select mhs.Kode_Mahasiswa, mhs.Nama_Mahasiswa
 from Mahasiswa mhs
 join Nilai nilai on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
 group by mhs.Kode_Mahasiswa,mhs.Nama_Mahasiswa
 having COUNT(nilai.Kode_Ujian)>1


 --no 8
 select Kode_Mahasiswa, Nama_Mahasiswa, Nama_Jurusan, agama.Deskripsi[Agama], Nama_Dosen, Status_Jurusan, td.Deskripsi[Deskripsi]
 from Mahasiswa mhs
 join Agama agama on agama.Kode_Agama = mhs.Kode_Agama
 join Jurusan jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
 join Dosen dosen on dosen.Kode_Jurusan = jur.Kode_Jurusan
 join Type_Dosen td on td.Kode_TypeDosen = dosen.Kode_Type_Dosen
 where Nama_Mahasiswa = 'Budi Gunawan'

 --no 9
 create view vwMahasiswa
 as
 select Kode_Mahasiswa, Nama_Mahasiswa, Nama_Jurusan, agama.Deskripsi[Agama], Nama_Dosen, Status_Jurusan, td.Deskripsi[Deskripsi]
 from Mahasiswa mhs
 join Agama agama on agama.Kode_Agama = mhs.Kode_Agama
 join Jurusan jur on jur.Kode_Jurusan = mhs.Kode_Jurusan
 join Dosen dosen on dosen.Kode_Jurusan = jur.Kode_Jurusan
 join Type_Dosen td on td.Kode_TypeDosen = dosen.Kode_Type_Dosen
 where Nama_Mahasiswa = 'Budi Gunawan'

 --no 10
  select *
 from Mahasiswa as mhs
 left join Nilai nilai on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa

 --no 11
	 --a
	 select Nama_Mahasiswa, Nilai[Maksimum]
	from Nilai nilai
	join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
	where Nilai = (select MAX(Nilai) from nilai)

	--b
	select Nama_Mahasiswa, Nilai[Maksimum]
	from Nilai nilai
	join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
	where Nilai = (select Min(Nilai) from nilai)

	--c
	select Nama_Mahasiswa, Nilai[Diatas rata-rata]
	from Nilai nilai
	join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
	where Nilai > (select AVG(Nilai) from nilai)

	--d
	select Nama_Mahasiswa, Nilai[Dibawah rata-rata]
	from Nilai nilai
	join Mahasiswa mhs on nilai.Kode_Mahasiswa = mhs.Kode_Mahasiswa
	where Nilai < (select AVG(Nilai) from nilai)

 --no 12
 alter table Jurusan alter column Biaya decimal(18,4)

 --no 13
 update Jurusan
 set Biaya = case Kode_Jurusan
 	when 'J001' then 1500000
	when 'J002' then 1550000
	when 'J003' then 1475000
	when 'J004' then 1350000
	when 'J005' then 1535000
	else Biaya
	end
where Nama_Jurusan in ('J001', 'J002', 'J003', 'J004', 'J005' )

-- no 14
select Nama_Jurusan, Biaya[Maksimum]
from Jurusan
where Biaya = (select MAX(Biaya) from Jurusan)

select Nama_Jurusan, Biaya[Maksimum]
from Jurusan
where Biaya = (select MIN(Biaya) from Jurusan)

select Nama_Jurusan, Biaya[Diatas rata-rata]
from Jurusan jurusan
where Biaya > (select AVG(Biaya) from Jurusan)

select Nama_Jurusan, Biaya[Dibawah rata-rata]
from Jurusan jurusan
where Biaya < (select AVG(Biaya) from Jurusan)





