--GROUP 01
--1. Tampilkan hasil penjualan Outlet Jaktim Jaya berdasarkan hari di tahun 2022
SELECT (s.Quantity * p.Harga) Penjualan, DAY(s.SellingDate) Hari, YEAR(s.SellingDate) Tahun
FROM Selling s
JOIN Product p ON s.KodeProduct = p.Kode
JOIN Outlet o ON s.KodeOutlet = o.Kode
WHERE o.Kode = 'O001' AND YEAR(s.SellingDate) = 2022
ORDER BY DAY(s.SellingDate)
--2. Tampilkan nama produk & jumlah penjualan produk dari outlet Jakpus Jaya
SELECT p.Nama, SUM(s.Quantity * p.Harga) Penjualan
FROM Selling s
JOIN Product p ON s.KodeProduct = p.Kode
JOIN Outlet o ON s.KodeOutlet = o.Kode
WHERE o.Kode = 'O005'
GROUP BY p.Nama

--3. Tampilkan nama produk yang terjual di bulan Januari
SELECT YEAR(s.SellingDate) Tahun, MONTH(s.SellingDate) Bulan, p.Nama
FROM Selling s
JOIN Product p ON s.KodeProduct = p.Kode
WHERE MONTH(s.SellingDate) = 1

--GROUP 02
--1 Tampilkan nama barang, harga barang dan tambahkan kolom dengan keterangan, jika diatas rata-rata maka upper, jika dibawah rata2 maka lower
SELECT AVG(Harga) FROM dbo.Product
SELECT
	p.Nama NamaProduk,
	p.Harga HargaProduk,
	CASE
		WHEN p.Harga > (SELECT AVG(Harga) FROM dbo.Product) THEN 'UPPER'
		WHEN p.Harga = (SELECT AVG(Harga) FROM dbo.Product) THEN 'SAMER'
		WHEN p.Harga < (SELECT AVG(Harga) FROM dbo.Product) THEN 'LOWER'
	END Keterangan
FROM dbo.Product p

--2 Tampilkan Produk yang terjual di bulan dan hari yang sama

SELECT
	s.SellingDate Tanggal,
	p.Nama NamaProduk
FROM dbo.Selling s
JOIN dbo.Product p
	ON p.Kode = s.KodeProduct
JOIN
(
	SELECT
		freq.Tanggal
	FROM
	(
		SELECT
			s.SellingDate Tanggal,
			COUNT(s.SellingDate) Banyak
		FROM dbo.Selling s
		GROUP BY
			s.SellingDate
	) freq
	WHERE
		freq.Banyak > 1
) freq ON freq.Tanggal = s.SellingDate


select * from dbo.Selling

--3 Tampilkan Frekuensi terjualnya suatu produk dengan menampilkan tanggal dan quantitynya

SELECT
	p.Nama NamaProduk,
	DAY(s.SellingDate) Tanggal,
	SUM(s.Quantity) Kuantiti,
	COUNT(DAY(s.SellingDate)) Frekuensi
FROM dbo.Selling s
JOIN dbo.Product p
	ON p.Kode = s.KodeProduct
GROUP BY
	p.Kode,
	p.Nama,
	DAY(s.SellingDate)

select * from Selling


-- GROUP 04
--1. tampilkan harga produk termurah perprovinsi
SELECT mhPro.NamaProv, mhPro.NamaProduk, mhPro.Harga
FROM MinHargaProv mhPro
	JOIN 
	(
	SELECT KodeProv, NamaProv, AVG(Harga) Harga
		FROM MinHargaProv
		GROUP BY KodeProv, NamaProv
	)mhp
	ON mhPro.KodeProv = mhp.KodeProv and  mhPro.Harga = mhp.Harga

CREATE VIEW MinHargaProv
AS
SELECT Provinsi.Kode KodeProv, Provinsi.Nama NamaProv, Product.Nama NamaProduk, AVG(Product.Harga) Harga
FROM Product
JOIN Selling sell ON sell.KodeProduct = Product.Kode
JOIN Outlet ON Outlet.Kode = sell.KodeOutlet
JOIN Kota ON Kota.Kode = Outlet.KodeKota
JOIN Provinsi ON Provinsi.Kode = Kota.KodeProvinsi
GROUP BY Provinsi.Kode, Provinsi.Nama, Product.Nama, Harga
order by Provinsi.Kode


--2. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
SELECT Outlet.Nama, Product.Nama, Product.Harga, SUM(sell.Quantity)JumlahProduk, SUM(sell.Quantity*Product.Harga)JumlahPenjualan
FROM Outlet
JOIN Selling sell ON Outlet.Kode = sell.KodeOutlet
JOIN Product ON sell.KodeProduct = Product.Kode
WHERE Product.Nama IN ('roti','pasta gigi', 'seblak')
GROUP BY Product.Nama, Outlet.Nama, Product.Harga, Outlet.Kode
ORDER BY Outlet.Kode


--3. Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT CONCAT(SUBSTRING(NamaKota, 1,3), SUBSTRING(NamaKota, CHARINDEX(' ',NamaKota,1) + 1,3), ' Jaya') NamaOutlet
FROM(
SELECT  
	Kota.Nama NamaKota
FROM Kota 
LEFT JOIN Outlet ON Outlet.KodeKota = Kota.Kode
WHERE KodeKota IS NULL AND Kota.Nama LIKE '%Jakarta%'
) NamaOutlet