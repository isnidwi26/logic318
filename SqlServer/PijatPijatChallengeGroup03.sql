DROP DATABASE db_PijatPijat
CREATE DATABASE db_PijatPijat



--00 Tampilkan rekapitulasi

CREATE VIEW vwRekapitulasi
AS
SELECT DISTINCT
		ref.Id [No],
		ref.Tanggal [Date],
		ref.Kode [Reff],
		ref.KodePemesanan [Book No],
		bo.Tanggal [Tanggal],
		agt.IdAnggota [MemberId],
		agt.Nama [Nama Customer],
		agt.Alamat [Alamat],
		agt.Kota [Kota],
		agt.Provinsi [Provinsi],
		ka.Nama [Grade],
		tp.Kode [KodeTypeP],
		tp.Nama [TypeP],
		pj.Kode [KodeType],
		pj.Nama [Type],
		ISNULL(trp.Nama,'') [Masanger],
		ksr.Nama Kasir,
		trs.Kuantiti,
		pj.Harga [Price],
		pj.Harga*tp.Pelayanan/100*trs.Kuantiti [Service],
		pj.Harga*tp.Pajak/100*trs.Kuantiti [Tax],
		((pj.Harga*trs.Kuantiti)+(pj.Harga*tp.Pelayanan/100*trs.Kuantiti) + (pj.Harga*tp.Pajak/100*trs.Kuantiti)) [Payment]
	FROM dbo.Pemesanan bo
	JOIN dbo.Referensi ref
		ON ref.KodePemesanan = bo.Kode
	JOIN dbo.Transaksi trs
		ON trs.KodeReferensi = ref.Kode
	JOIN dbo.Anggota agt
		ON agt.IdAnggota = bo.KodeAnggota
	JOIN dbo.Keanggotaan ka
		ON ka.Kode = agt.KodeKeanggotaan
	JOIN dbo.ProdukJasa pj
		ON pj.Kode = trs.KodeProdukJasa
	JOIN dbo.TipeProduk tp
		ON tp.Kode = pj.KodeTipe
	JOIN
		(
			SELECT
				pgw.IdPegawai,
				pgw.Nama
			FROM dbo.Pegawai pgw
			JOIN dbo.Referensi ref
				ON ref.KodeKasir = pgw.IdPegawai
		) ksr ON ksr.IdPegawai = ref.KodeKasir
	LEFT JOIN
		(
			SELECT
				pgw.IdPegawai,
				pgw.Nama
			FROM dbo.Pegawai pgw
			JOIN dbo.Transaksi trs
				ON trs.KodeTerapis = pgw.IdPegawai
		) trp ON trp.IdPegawai = trs.KodeTerapis
ORDER BY
	ref.Kode ASC,
	tp.Nama DESC

SELECT * FROM vwRekapitulasi

--01 Tampilkan Masseus dengan pelanggan terbanyak
CREATE VIEW vwMasseus
AS
SELECT 
 Masanger Terapis,
 COUNT(r.Reff) [JumlahCustomer]
FROM vwRekapitulasi r
WHERE r.Masanger != ''
GROUP BY Masanger

SELECT 
	m.Terapis,
	maks.JumlahCustomer
FROM vwMasseus m
JOIN 
(
	SELECT 
		MAX(JumlahCustomer) JumlahCustomer
	FROM vwMasseus
)maks ON maks.JumlahCustomer = m.JumlahCustomer
GROUP BY m.Terapis, m.JumlahCustomer, maks.JumlahCustomer

--02 Tampilkan Produk goods yang paling laris

CREATE VIEW vwHitTerjual
AS
SELECT 
		pijat.KodeTypeP [KodeTipe],
		pijat.KodeType,
		pijat.[Type] [NamaTipe],
		SUM(pijat.Kuantiti) Terjual
	FROM vwRekapitulasi pijat
	WHERE pijat.KodeTypeP = 'TP02'
	GROUP BY pijat.KodeTypeP, pijat.Type, pijat.KodeType

SELECT 
	ht.NamaTipe Nama,
	ht.Terjual Terlaris
FROM TipeProduk tp
JOIN vwHitTerjual ht ON tp.Kode = ht.KodeTipe
WHERE ht.Terjual = (SELECT MAX(Terjual) FROM vwHitTerjual)

--03 Tampilkan service massage yang paling sedikit peminat
CREATE VIEW vwHitTerjualMassage
AS
SELECT 
		pijat.KodeTypeP [KodeTipe],
		pijat.KodeType,
		pijat.[Type] [NamaTipe],
		SUM(pijat.Kuantiti) Terjual
	FROM vwRekapitulasi pijat
	WHERE pijat.KodeTypeP = 'TP01'
	GROUP BY pijat.KodeTypeP, pijat.Type, pijat.KodeType


SELECT
	htm.NamaTipe Nama,
	htm.Terjual SedikitPeminat
FROM TipeProduk tp
JOIN vwHitTerjualMassage htm ON tp.Kode = htm.KodeTipe
WHERE htm.Terjual = (SELECT MIN(Terjual) FROM vwHitTerjualMassage)

--04 Tampilkan jumlah service setiap bulannya.
SELECT 
	jml.Tahun Tahun,
	jml.Bulan Bulan,
	SUM(jml.Jumlah) Jumlah
FROM
(
	SELECT 
		YEAR(vwRekapitulasi.Date) Tahun,
		MONTH(vwRekapitulasi.Date) Bulan,
		COUNT(vwRekapitulasi.TypeP) Jumlah
	FROM 
		vwRekapitulasi
	WHERE vwRekapitulasi.KodeTypeP = 'TP01'
	GROUP BY vwRekapitulasi.Date
) jml
GROUP BY jml.Bulan, jml.Tahun

--05 Tampilkan tahun bulan penjualan terbanyak
CREATE VIEW vwPenjualanTerbanyak
AS
SELECT
	YEAR(p.Date) TahunPemesanan,
	MONTH(p.Date) BulanPemesanan, 
	SUM(p.Payment) jmlPenjualan
FROM vwRekapitulasi p
GROUP BY MONTH(p.Date), YEAR(p.Date)

SELECT TOP 1 *
FROM vwPenjualanTerbanyak pt
ORDER BY pt.jmlPenjualan DESC

--06 Tampilkan masing-masing total pendapatan service/massage & product/goods.
SELECT 
	YEAR(pijat.Date) Tahun,
	MONTH(pijat.Date) Bulan,
	TypeP [Type],
	SUM(pijat.[Payment]) Pendapatan
FROM 
	vwRekapitulasi pijat
GROUP BY TypeP, YEAR(pijat.Date), MONTH(pijat.Date)


--07 Dalam meningkatkan minat pelanggan, diadakan promo untuk para member dan mendapat potongan.
--		Bronze 2.5% untuk service tertentu
--		Silver 5% untuk service tertentu
--		Gold 5% untuk service & product tertentu
--		Platinum 8% untuk service & product tertentu
--	a. Tambahkan tabel dan/atau kolom diskon per grade dan product.


	ALTER TABLE dbo.Keanggotaan
	ADD Diskon DECIMAL(18,4)

	ALTER TABLE dbo.Keanggotaan
	ADD DiskonJasa INT

	ALTER TABLE dbo.Keanggotaan
	ADD DiskonBarang INT

	UPDATE dbo.Keanggotaan SET Diskon = 2.5 WHERE Kode = 'MB01'
	UPDATE dbo.Keanggotaan SET DiskonJasa = 1 WHERE Kode = 'MB01'
	UPDATE dbo.Keanggotaan SET DiskonBarang = 0 WHERE Kode = 'MB01'

	UPDATE dbo.Keanggotaan SET Diskon = 5 WHERE Kode = 'MB02'
	UPDATE dbo.Keanggotaan SET DiskonJasa = 1 WHERE Kode = 'MB02'
	UPDATE dbo.Keanggotaan SET DiskonBarang = 0 WHERE Kode = 'MB02'

	UPDATE dbo.Keanggotaan SET Diskon = 5 WHERE Kode = 'MB03'
	UPDATE dbo.Keanggotaan SET DiskonJasa = 1 WHERE Kode = 'MB03'
	UPDATE dbo.Keanggotaan SET DiskonBarang = 1 WHERE Kode = 'MB03'

	UPDATE dbo.Keanggotaan SET Diskon = 8 WHERE Kode = 'MB04'
	UPDATE dbo.Keanggotaan SET DiskonJasa = 1 WHERE Kode = 'MB04'
	UPDATE dbo.Keanggotaan SET DiskonBarang = 1 WHERE Kode = 'MB04'
--	b. Tampilkan transaksi dengan diskon untuk customer sesuai grade-nya.
	
	SELECT
		trs.Kuantiti [Kuantiti],
		pj.Harga,
		pj.Harga * trs.Kuantiti [Price],
		CASE
			WHEN tp.Kode = 'TP01' THEN (pj.Harga * trs.Kuantiti) * ka.Diskon * ka.DiskonJasa * (-1) / 100
			WHEN tp.Kode = 'TP02' THEN (pj.Harga * trs.Kuantiti) * ka.Diskon * ka.DiskonBarang * (-1) / 100
		END [Diskon],
		--pj.Harga * trs.Kuantiti * (100 - ka.Diskon) / 100 [Price (Diskon)],
		CASE
			WHEN tp.Kode = 'TP01' THEN (pj.Harga * trs.Kuantiti) * (100 - ka.Diskon * ka.DiskonJasa) / 100 * tp.Pelayanan / 100
			WHEN tp.Kode = 'TP02' THEN (pj.Harga * trs.Kuantiti) * (100 - ka.Diskon * ka.DiskonBarang) / 100 * tp.Pelayanan / 100
		END [Service],
		CASE
			WHEN tp.Kode = 'TP01' THEN (pj.Harga * trs.Kuantiti) * (100 - ka.Diskon * ka.DiskonJasa) / 100 * tp.Pajak / 100
			WHEN tp.Kode = 'TP02' THEN (pj.Harga * trs.Kuantiti) * (100 - ka.Diskon * ka.DiskonBarang) / 100 * tp.Pajak / 100
		END [Tax],
		CASE
			WHEN tp.Kode = 'TP01' THEN (pj.Harga * trs.Kuantiti * (100 - ka.Diskon * ka.DiskonJasa) / 100) * (100 + tp.Pelayanan + tp.Pajak) / 100
			WHEN tp.Kode = 'TP02' THEN (pj.Harga * trs.Kuantiti * (100 - ka.Diskon * ka.DiskonBarang) / 100) * (100 + tp.Pelayanan + tp.Pajak) / 100
		END [Total]
	FROM dbo.Transaksi trs
	JOIN dbo.ProdukJasa pj
		ON pj.Kode = trs.KodeProdukJasa
	JOIN dbo.TipeProduk tp
		ON tp.Kode = pj.KodeTipe
	JOIN dbo.Referensi ref
		ON ref.Kode = trs.KodeReferensi
	JOIN dbo.Pemesanan bo
		ON bo.Kode = ref.KodePemesanan
	JOIN dbo.Anggota agt
		ON agt.IdAnggota = bo.KodeAnggota
	JOIN dbo.Keanggotaan ka
		ON ka.Kode = agt.KodeKeanggotaan

--08 Tambahkan jenis kelamin untuk member/pelanggan & untuk Masseus, Jenis kelamin char(1) L/P
	--ALTER TABLE Anggota ADD JenisKelamin VARCHAR(1)
	--sudah ada namanya gender

	SELECT
	agt.Nama,
	agt.Gender,
	'Anggota' [Status]
	FROM dbo.Anggota agt
	UNION
	SELECT
		pgw.Nama,
		pgw.Gender,
		'Pegawai' [Status]
	FROM dbo.Pegawai pgw
	WHERE pgw.RolePegawai = 'EMP01'

--09 Tampilkan jumlah customer per Masseus dan bonus dimana Jumlah customer 5-10 mendapat 5% dari harga service, sedangkan >10 mendapat 8% dari harga serice.
	
	CREATE VIEW vwTerapis
	AS
		SELECT
			pgw.IdPegawai,
			pgw.Nama,
			COUNT(trs.KodeTerapis) BanyakPraktek
		FROM dbo.Transaksi trs
		JOIN dbo.Pegawai pgw
			ON pgw.IdPegawai = trs.KodeTerapis
		GROUP BY
			pgw.IdPegawai,
			pgw.Nama

	CREATE VIEW vwRekapTerapis
	AS
		SELECT
			pgw.IdPegawai,
			pgw.Nama NamaPegawai,
			pj.Kode KodeJasa,
			pj.Nama NamaJasa,
			pj.Harga
		FROM dbo.Transaksi trs
		JOIN dbo.Pegawai pgw
			ON trs.KodeTerapis = pgw.IdPegawai
		JOIN dbo.ProdukJasa pj
			ON pj.Kode = trs.KodeProdukJasa
		--ORDER BY
		--	pgw.IdPegawai

	CREATE VIEW vwBonusTerapis
	AS
	SELECT
		vrt.IdPegawai,
		vrt.NamaPegawai,
		vrt.KodeJasa,
		vrt.NamaJasa,
		vrt.Harga,
		CASE
			WHEN vt.BanyakPraktek < 5 THEN vrt.Harga * 0.00
			WHEN vt.BanyakPraktek >= 5 AND vt.BanyakPraktek <= 10 THEN vrt.Harga * 0.05
			WHEN vt.BanyakPraktek > 10 THEN vrt.Harga * 0.08
		END [Bonus]
	FROM vwRekapTerapis vrt
	JOIN vwTerapis vt
		ON vt.IdPegawai = vrt.IdPegawai
	--ORDER BY vrt.NamaPegawai

	SELECT
		vbt.NamaPegawai,
		COUNT(vbt.IdPegawai) BanyakPraktek,
		SUM(vbt.Harga) Pendapatan,
		SUM(vbt.Bonus) Bonus
	FROM vwBonusTerapis vbt
	GROUP BY
		vbt.IdPegawai,
		vbt.NamaPegawai

	--JAWABAN
	SELECT
		byk.NamaJasa,
		byk.BanyakDipakai,
		CASE
			WHEN byk.BanyakDipakai < 5 THEN 0 * byk.BanyakDipakai * pj.Harga / 100
			WHEN byk.BanyakDipakai >= 5 AND byk.BanyakDipakai <= 10 THEN 5 * byk.BanyakDipakai * pj.Harga / 100
			WHEN byk.BanyakDipakai > 10 THEN 8 * byk.BanyakDipakai * pj.Harga / 100
		END Bonus
	FROM
	(
		SELECT
			pj.Kode,
			pj.Nama NamaJasa,
			COUNT(pj.Kode) BanyakDipakai
		FROM dbo.ProdukJasa pj
		LEFT JOIN dbo.Transaksi trs
			ON pj.Kode = trs.KodeProdukJasa
		LEFT JOIN dbo.TipeProduk tp
			ON tp.Kode = pj.KodeTipe
		WHERE tp.Kode = (SELECT Kode FROM dbo.TipeProduk WHERE Nama LIKE '%massage%')
		GROUP BY
			pj.Kode,
			pj.Nama
	) byk
	JOIN dbo.ProdukJasa pj
		ON pj.Kode = byk.Kode


--10 Tampilkan tren jumlah customer harian dengan nama hari (Sunday-Saturday) segala waktu.

CREATE VIEW vwHari
AS
	SELECT
	1 [VALUE], 'Sunday' Nama
	UNION
	SELECT
	2 [VALUE], 'Monday' Nama
	UNION
	SELECT
	3 [VALUE], 'Tuesday' Nama
	UNION
	SELECT
	4 [VALUE], 'Wednesday' Nama
	UNION
	SELECT
	5 [VALUE], 'Thursday' Nama
	UNION
	SELECT
	6 [VALUE], 'Friday' Nama
	UNION
	SELECT
	7 [VALUE], 'Saturday' Nama

SELECT * FROM vwHari

--JAWABAN
SELECT
	ref.Hari,
	COUNT(ref.Kode) BanyakPelanggan
FROM
(
	SELECT
		ref.Kode,
		DATENAME(WEEKDAY,ref.Tanggal) Hari
	FROM dbo.Transaksi trs
	JOIN dbo.Referensi ref
		ON ref.Kode = trs.KodeReferensi
	GROUP BY
		ref.Kode,
		ref.Tanggal
) ref
JOIN vwHari vh
	ON vh.Nama = ref.Hari
GROUP BY
	ref.Hari,
	vh.[VALUE]
ORDER BY
	vh.[VALUE]
