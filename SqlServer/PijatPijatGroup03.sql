CREATE DATABASE db_PijatPijat

CREATE TABLE Keanggotaan
(
	Id INT IDENTITY(1,1),
	Kode VARCHAR(10) NOT NULL,
	Nama VARCHAR(50) NOT NULL,
	PRIMARY KEY (Kode)
)

CREATE TABLE Anggota
(
	Id INT IDENTITY(1,1),
	IdAnggota VARCHAR(10) NOT NULL,
	Nama VARCHAR(50) NOT NULL,
	Gender VARCHAR(1) NOT NULL,
	Alamat VARCHAR(200) NOT NULL,
	Kota VARCHAR(50) NOT NULL,
	Provinsi VARCHAR(50) NOT NULL,
	KodeKeanggotaan VARCHAR(10) NOT NULL
	PRIMARY KEY (IdAnggota)
	FOREIGN KEY (KodeKeanggotaan) REFERENCES Keanggotaan(Kode)
)

CREATE TABLE RolePegawai
(	
	Id INT IDENTITY(1,1),
	Kode VARCHAR(10) NOT NULL,
	Nama VARCHAR(50) NOT NULL
	PRIMARY KEY (Kode)
)

CREATE TABLE Pegawai
(
	Id INT IDENTITY(1,1),
	IdPegawai VARCHAR(10) NOT NULL,
	Nama VARCHAR(50) NOT NULL,
	Gender VARCHAR(1) NOT NULL,
	RolePegawai VARCHAR(10) NOT NULL
	PRIMARY KEY (IdPegawai)
	FOREIGN KEY (RolePegawai) REFERENCES RolePegawai(Kode)
)

CREATE TABLE TipeProduk
(
	Id INT IDENTITY(1,1),
	Kode VARCHAR(10) NOT NULL,
	Nama VARCHAR(50) NOT NULL,
	PersenPelayanan DECIMAL(4,2) NOT NULL,
	PersenPajak DECIMAL(4,2) NOT NULL
	PRIMARY KEY (Kode)
)


CREATE TABLE ProdukJasa
(
	Id INT IDENTITY(1,1),
	Kode VARCHAR(10) NOT NULL,
	Nama VARCHAR(50) NOT NULL,
	Harga DECIMAL(18,4) NOT NULL,
	KodeTipe VARCHAR(10) NOT NULL
	PRIMARY KEY (Kode)
	FOREIGN KEY (KodeTipe) REFERENCES TipeProduk(Kode)
)

CREATE TABLE Pemesanan
(
	Id INT IDENTITY(1,1),
	Tanggal DATE NOT NULL,
	Kode VARCHAR(10) NOT NULL,
	KodeAnggota VARCHAR(10) NOT NULL
	PRIMARY KEY (Kode)
	FOREIGN KEY (KodeAnggota) REFERENCES Anggota(IdAnggota)
)

CREATE TABLE Referensi
(	
	Id INT IDENTITY(1,1),
	Tanggal DATE NOT NULL,
	Kode VARCHAR(10) NOT NULL,
	KodePemesanan VARCHAR(10) NOT NULL,
	KodeKasir VARCHAR(10) NOT NULL
	PRIMARY KEY (Kode)
	FOREIGN KEY (KodePemesanan) REFERENCES Pemesanan(Kode),
	FOREIGN KEY (KodeKasir) REFERENCES Pegawai(IdPegawai)
)

CREATE TABLE Transaksi
(
	Id INT IDENTITY(1,1),
	KodeReferensi VARCHAR(10) NOT NULL,
	KodeTerapis VARCHAR(10),
	KodeProdukJasa VARCHAR(10) NOT NULL,
	Kuantiti INT NOT NULL
	PRIMARY KEY (Id)
	FOREIGN KEY (KodeReferensi) REFERENCES Referensi(Kode),
	FOREIGN KEY (KodeTerapis) REFERENCES Pegawai(IdPegawai),
	FOREIGN KEY (KodeProdukJasa) REFERENCES ProdukJasa(Kode)
)

INSERT INTO Keanggotaan VALUES
('MB01', 'Bronze'),
('MB02', 'Silver'),
('MB03', 'Gold'),
('MB04', 'Platinum')

INSERT INTO Anggota VALUES
('M0982', 'Bayu', 'L', 'Jl. Garuda', 'Tangerang Selatan', 'Banten', 'MB03'),
('M0983', 'Uus Man', 'L', 'Jl. Gagak', 'Jakarta Selatan', 'Jakarta', 'MB01'),
('M0984', 'Al Fi', 'L', 'Jl. Elang', 'Bekasi Selatan', 'Bekasi', 'MB01'),
('M0985', 'Den Aldi T.', 'L', 'Jl. Falcon', 'Bandung Selatan', 'Bandung', 'MB02'),
('M0986', 'Is Me', 'L', 'Jl. Kolibri', 'Depok Selatan', 'Depok', 'MB04')

INSERT INTO RolePegawai VALUES
('EMP01', 'Terapis'),
('EMP02', 'Kasir')

INSERT INTO Pegawai VALUES
('E0001', 'Wahyu', 'L', 'EMP01'),
('E0002', 'Nino', 'L', 'EMP02'),
('E0003', 'Andy', 'L', 'EMP01'),
('E0004', 'Budy', 'L', 'EMP01'),
('E0005', 'Cindy', 'P', 'EMP01'),
('E0006', 'Nina', 'P', 'EMP02')

INSERT INTO TipeProduk VALUES
('TP01', 'Massage', 0.05, 0.11),
('TP02', 'Goods', 0, 0.11)

INSERT INTO ProdukJasa VALUES
('J001', 'Full Body', 120000, 'TP01'),
('J002', 'Punggung', 50000, 'TP01'),
('J003', 'Kaki', 50000, 'TP01'),
('J004', 'Tangan', 50000, 'TP01'),
('J005', 'Akupuntur', 500000, 'TP01'),
('J006', 'Bekam', 50000, 'TP01'),
('P001', 'Minyak Terapi', 25000, 'TP02'),
('P002', 'Wedang Jahe', 15000, 'TP02'),
('P003', 'Handuk', 46500, 'TP02')

INSERT INTO Pemesanan VALUES
('20230104', 'B098', 'M0982'),
('20230104', 'B099', 'M0986'),
('20230105', 'B100', 'M0983'),
('20230107', 'B101', 'M0984'),
('20230110', 'B102', 'M0985'),
('20230110', 'B103', 'M0986'),
('20230112', 'B104', 'M0983')

INSERT INTO Referensi VALUES
('20230105', 'M23010923', 'B098', 'E0002'),
('20230106', 'M23010924', 'B099', 'E0002'),
('20230107', 'M23010925', 'B100', 'E0002'),
('20230108', 'M23010926', 'B101', 'E0002'),
('20230111', 'M23010927', 'B102', 'E0002'),
('20230112', 'M23010928', 'B103', 'E0002'),
('20230112', 'M23010929', 'B104', 'E0006')

INSERT INTO Transaksi VALUES
('M23010923', 'E0001', 'J001', 1),
('M23010923',    NULL, 'P001', 1),
('M23010923',    NULL, 'P002', 1),
('M23010923',    NULL, 'P003', 1),
('M23010924', 'E0005', 'J001', 1),
('M23010924',    NULL, 'P001', 1),
('M23010925', 'E0003', 'J005', 1),
('M23010925',    NULL, 'P002', 2),
('M23010926', 'E0004', 'J006', 1),
('M23010927', 'E0003', 'J002', 1),
('M23010928', 'E0005', 'J001', 1),
('M23010928',    NULL, 'P001', 1),
('M23010929',    NULL, 'P002', 3)



SELECT DISTINCT
	al.[No],
	al.[Date],
	al.[Reff],
	al.[Book No],
	al.[Tanggal],
	al.[MemberId],
	al.[Nama Customer],
	al.[Alamat],
	al.[Kota],
	al.[Provinsi],
	al.[Grade],
	al.[TypeP] [Type],
	al.[TypeProduk] [Type],
	ISNULL(trp.Nama, '') [Masanger],
	ksr.Nama [Kasir],
	al.[Kuantiti],
	al.[Price],
	al.[Price]*al.[PerPel]*al.Kuantiti [Service],
	al.[Price]*al.[PerPa]*al.Kuantiti [Tax],
	(al.[Price]+(al.[Price]*al.[PerPel]*al.Kuantiti) + (al.[Price]*al.[PerPa]*al.Kuantiti)) [Payment]
FROM
(
	SELECT
		ref.Id [No],
		ref.Tanggal [Date],
		ref.Kode [Reff],
		ref.KodePemesanan [Book No],
		bo.Tanggal [Tanggal],
		agt.IdAnggota [MemberId],
		agt.Nama [Nama Customer],
		agt.Alamat [Alamat],
		agt.Kota [Kota],
		agt.Provinsi [Provinsi],
		ka.Nama [Grade],
		tp.Nama [TypeP],
		tp.PersenPajak [PerPa],
		tp.PersenPelayanan [PerPel],
		pj.Nama [TypeProduk],
		trs.KodeTerapis [Masanger],
		ref.KodeKasir [Kasir],
		trs.Kuantiti,
		pj.Harga [Price]

	FROM dbo.Pemesanan bo
	JOIN dbo.Referensi ref
		ON ref.KodePemesanan = bo.Kode
	JOIN dbo.Transaksi trs
		ON trs.KodeReferensi = ref.Kode
	JOIN dbo.Anggota agt
		ON agt.IdAnggota = bo.KodeAnggota
	JOIN dbo.Keanggotaan ka
		ON ka.Kode = agt.KodeKeanggotaan
	JOIN dbo.ProdukJasa pj
		ON pj.Kode = trs.KodeProdukJasa
	JOIN dbo.TipeProduk tp
		ON tp.Kode = pj.KodeTipe
) al
JOIN
(
	SELECT
		pgw.IdPegawai,
		pgw.Nama
	FROM dbo.Pegawai pgw
	JOIN dbo.Referensi ref
		ON ref.KodeKasir = pgw.IdPegawai
) ksr ON ksr.IdPegawai = al.Kasir
LEFT JOIN
(
	SELECT
		pgw.IdPegawai,
		pgw.Nama
	FROM dbo.Pegawai pgw
	JOIN dbo.Transaksi trs
		ON trs.KodeTerapis = pgw.IdPegawai
) trp ON trp.IdPegawai = al.Masanger
ORDER BY
	al.Reff ASC,
	al.TypeP DESC




