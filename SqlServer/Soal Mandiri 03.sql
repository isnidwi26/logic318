--Tampilkan Kode Produk, Nama Produk dan  jumlah produk diatas rata-rata yang nama produknya berakhiran i dan mengubah Nama Produk menjadi huruf kapital
select Kode, Upper(Nama), sum(Quantity)[Diatas rata-rata]
from Product
join Selling on KodeProduct = Product.Kode
group by Nama, Kode
having SUM(Quantity) > (
	select 
		AVG(JumlahPenjualan) rataRata
	from
	(
		select
			SUM(quantity) JumlahPenjualan
		from Selling
		group by Selling.KodeProduct
	) TotalPerProduk
)
AND Nama LIKE '%i'
order by Nama asc


-- Tampilkan NamaProduk, Harga, dan JumlahTerjual, untuk produk yang belum pernah laku terjual

SELECT
	p.Nama NamaProduk,
	p.Harga,
	x.Terjual
FROM Product p
JOIN
(
	SELECT
		p.Kode,
		ISNULL(COUNT(s.Id), 0) Terjual
	FROM Product p
	LEFT JOIN dbo.Selling s
		ON p.Kode = s.KodeProduct
	GROUP BY
		p.Kode,
		s.Id
) x 
ON 
	x.Kode = p.Kode
WHERE
	x.Terjual = 0

--cara pendek
SELECT p.Nama, p.Harga, ISNULL(sell.Quantity, 0) Terjual
FROM Product p
LEFT JOIN Selling sell ON p.Kode = sell.KodeProduct
WHERE sell.Id IS NULL


-- Tampilkan NamaKota dan JumlahTransaksi, untuk kota yang belum pernah terjadi transaksi

SELECT
	k.Nama NamaKota,
	x.Terjual JumlahTransaksi
FROM dbo.Kota k
JOIN
(
	SELECT
		k.Kode,
		ISNULL(COUNT(s.Id), 0) Terjual
	FROM dbo.Kota k
	LEFT JOIN dbo.Outlet o
		ON o.KodeKota = k.Kode
	LEFT JOIN dbo.Selling s
		ON s.KodeOutlet = o.Kode
	GROUP BY
		k.Kode,
		s.Id
) x ON x.Kode = k.Kode
WHERE
	x.Terjual = 0

--CARA LAIN
SELECT
	Kota.Kode,
	Kota.Nama,
	COUNT(sell.Referensi) JumlahTransaksi
FROM Selling sell
RIGHT JOIN Outlet ON sell.KodeOutlet = Outlet.Kode
RIGHT JOIN Kota ON Kota.Kode = Outlet.KodeKota
JOIN Provinsi ON Provinsi.Kode = Kota.KodeProvinsi
--WHERE sell.Id IS NULL
GROUP BY Kota.Kode, Kota.Nama
HAVING COUNT(sell.Referensi) = 0


--Tampilkan Jumlah Penjualan Produk Terbanyak Antara Bulan 1 - 5 Tahun 2022
--OUTPUT:
--Roti 112500 2022-01-01
--Sandal 119000 2022-02-11
--Tissue 284000 2022-03-01
--Pasta Gigi 89000 2022-04-11
--Kaos Kaki 314450 2022-05-01

	CREATE VIEW omzet
AS
SELECT
	YEAR(s.SellingDate) Tahun,
	MONTH(s.SellingDate) Bulan,
	s.KodeProduct KodeProduk,
	SUM(s.Quantity * p.Harga) Penjualan
FROM dbo.Selling s
JOIN dbo.Product p
	ON p.Kode = s.KodeProduct
WHERE
	YEAR(s.SellingDate) = 2022 AND
	MONTH(s.SellingDate) >= 1 AND
	MONTH(s.SellingDate) <= 5
GROUP BY
	YEAR(s.SellingDate),
	MONTH(s.SellingDate),
	s.KodeProduct

SELECT
	omz.Tahun,
	omz.Bulan,
	p.Nama NamaProduk,
	omz.Penjualan
FROM omzet omz
JOIN dbo.Product p
	ON p.Kode = omz.KodeProduk
WHERE
	(omz.Bulan = 1 AND omz.Penjualan = (SELECT MAX(Penjualan) FROM omzet WHERE Bulan = 1)) OR
	(omz.Bulan = 2 AND omz.Penjualan = (SELECT MAX(Penjualan) FROM omzet WHERE Bulan = 2)) OR
	(omz.Bulan = 3 AND omz.Penjualan = (SELECT MAX(Penjualan) FROM omzet WHERE Bulan = 3)) OR
	(omz.Bulan = 4 AND omz.Penjualan = (SELECT MAX(Penjualan) FROM omzet WHERE Bulan = 4)) OR
	(omz.Bulan = 5 AND omz.Penjualan = (SELECT MAX(Penjualan) FROM omzet WHERE Bulan = 5))
ORDER BY
	omz.Bulan