--DDL

--1 Buatlah tabel Kota dengan kolom : Id increment, Kode varchar (20), dan Nama varchar (50)

CREATE TABLE Kota
(
	Id INT IDENTITY(1,1),
	Kode VARCHAR(20) NOT NULL,
	Nama VARCHAR(50) NOT NULL
)

DROP TABLE dbo.Kota

--2 Tambahkanlah constraint PK untuk tabel Kota kolom Kode

ALTER TABLE Kota
ADD CONSTRAINT PK_Kota PRIMARY KEY (Kode)

--3 Buatlah tabel Kecamatan dengan kolom: Id increment, Kode varchar(20), Nama varchar(50), dan KodeKota(10)

CREATE TABLE Kecamatan
(
	Id INT IDENTITY(1,1),
	Kode VARCHAR(20) NOT NULL,
	Nama VARCHAR(50) NOT NULL,
	KodeKota VARCHAR(10) NOT NULL
)

DROP TABLE Kecamatan

--4 Tambahkanlah constraint PK pada tabel Kecamatan kolom Kode, FK kolom KodeKota reference tabel Kota kolom Kode, dan ubahlah ukuran KodeKota menjadi 20

ALTER TABLE Kecamatan
ADD PRIMARY KEY(Kode)

ALTER TABLE Kecamatan
ALTER COLUMN KodeKota VARCHAR(20)

ALTER TABLE Kecamatan
ADD FOREIGN KEY (KodeKota) REFERENCES Kota(Kode)

--5 Tambahkan kolom KodeKecamatan varchar (20) pada tabel Halte dengan foreign key ke tabel Kecamatan kolom Kode

ALTER TABLE Halte
ADD KodeKecamatan VARCHAR(20)

ALTER TABLE Halte
ADD FOREIGN KEY (KodeKecamatan) REFERENCES Kecamatan(Kode)

--DML
--1 Tampilkan banyak transaksi yang terjadi dengan harga 0 yang dikelompokkan berdasarkan bulan dan tahun

SELECT
	rp0.Tahun,
	rp0.Bulan,
	COUNT(rp0.BanyakTransaksiRp0) BanyakTransaksiRp0
FROM
(
	SELECT
		YEAR(trs.JamTapIn) Tahun,
		MONTH(trs.JamTapIn) Bulan,
		COUNT(trs.KodeTarif) BanyakTransaksiRp0
	FROM dbo.Transaksi trs
	JOIN dbo.Tarif trf
		ON trf.Kode = trs.KodeTarif
	WHERE
		trs.KodeTarif = (SELECT trf.Kode FROM dbo.Tarif trf WHERE trf.Harga = 0)
	GROUP BY
		trs.JamTapIn
) rp0
GROUP BY
	rp0.Tahun,
	rp0.Bulan

--2 Tampilkan banyak transaksi yang terjadi, yang dilakukan oleh 1 orang, kurang dari 2 kali dalam 1 hari

SELECT
	SUM(t1h.Transaksi1Hari) Transaksi1Kali1Hari
FROM
(
	SELECT
		trs.KartuPengguna,
		YEAR(trs.JamTapIn) Tahun,
		MONTH(trs.JamTapIn) Bulan,
		DAY(trs.JamTapIn) Tanggal,
		COUNT(trs.JamTapIn) Transaksi1Hari
	FROM dbo.Transaksi trs
	GROUP BY
		YEAR(trs.JamTapIn),
		MONTH(trs.JamTapIn),
		DAY(trs.JamTapIn),
		trs.KartuPengguna
) t1h
WHERE
	t1h.Transaksi1Hari < 2

--3  Tampilkan banyak transaksi yang dilakukan sebelum jam 12 siang

SELECT
	COUNT(trs.KodeTransaksi) JumlahTransaksiSebelumJam12
FROM dbo.Transaksi trs
WHERE
	DATEDIFF(HOUR, CONVERT(DATETIME, CONCAT(YEAR(trs.JamTapIn),'-',MONTH(trs.JamTapIn),'-',DAY(trs.JamTapIn))), trs.JamTapIn) < 12

--4  Tampilkan 3 tanggal dengan pendapatan terbanyak

SELECT TOP 3
	tgl.Tahun,
	tgl.Bulan,
	tgl.Tanggal,
	SUM(trf.Harga) TotalPendapatan
FROM
(
	SELECT
		YEAR(trs.JamTapIn) Tahun,
		MONTH(trs.JamTapIn) Bulan,
		DAY(trs.JamTapIn) Tanggal,
		trs.KodeTarif
	FROM dbo.Transaksi trs
) tgl
JOIN dbo.Tarif trf
	ON trf.Kode = tgl.KodeTarif
GROUP BY
	tgl.Tahun,
	tgl.Bulan,
	tgl.Tanggal
ORDER BY
	SUM(trf.Harga) DESC

--5  Tampilkan banyak transaksi yang terjadi dengan durasi kurang dari 1 jam (dan nominal pendapatannya)

SELECT
	COUNT(trs.KodeTransaksi) BanyakTransaksiKurangDari1Jam,
	SUM(trf.Harga) JumlahPendapatan
FROM dbo.Transaksi trs
JOIN dbo.Tarif trf
	ON trf.Kode = trs.KodeTarif
WHERE
	DATEDIFF(HOUR, trs.JamTapIn, trs.JamTapOut) < 1

--6  Tampilkan 3 kartupengguna dengan jumlah (nominal) tarif terbanyak

SELECT TOP 3
	trs.KartuPengguna,
	SUM(trf.Harga) NominalTransaksi
FROM dbo.Transaksi trs
JOIN dbo.Tarif trf
	ON trf.Kode = trs.KodeTarif
GROUP BY
	trs.KartuPengguna
ORDER BY
	SUM(trf.Harga) DESC

--7  Tampilkan daftar kartupengguna yang pernah menggunakan seluruh jenis tarif

SELECT
	abc.KartuPengguna
FROM
(
	SELECT
		trs.KartuPengguna,
		trs.KodeTarif
	FROM dbo.Transaksi trs
	RIGHT JOIN dbo.Tarif trf
		ON trf.Kode = trs.KodeTarif
	GROUP BY
		trs.KartuPengguna,
		trs.KodeTarif
) abc
GROUP BY
	abc.KartuPengguna
HAVING
	COUNT(abc.KodeTarif) = (SELECT COUNT(Kode) FROM dbo.Tarif)

--8  Tampilkan nomorkartu dan jumlah transaksinya untuk nama hari dengan jumlah transaksi terbanyak

CREATE VIEW no08
AS
	SELECT
		DATENAME(WEEKDAY, trs.JamTapIn) NamaHari,
		COUNT(DATENAME(WEEKDAY, trs.JamTapIn)) JumlahHari
	FROM dbo.Transaksi trs
	GROUP BY
		DATENAME(WEEKDAY, trs.JamTapIn)
				
SELECT
	trs.KartuPengguna,
	COUNT(trs.KartuPengguna) JumlahTransaksi
FROM dbo.Transaksi trs
WHERE
	DATENAME(WEEKDAY, trs.JamTapIn) = (SELECT NamaHari FROM no08 WHERE no08.JumlahHari = (SELECT MAX(no08.JumlahHari) FROM no08))
GROUP BY
	trs.KartuPengguna	

--9 Tampilkan halte dengan bis terbanyak (halte yang dapat disinggahi oleh bis dengan variasi kode bis terbanyak)

CREATE VIEW no09
AS
	SELECT
		h.Kode KodeHalte,
		COUNT(b.Kode) BanyakBis
	FROM dbo.Jalur j
	JOIN dbo.Halte h
		ON h.Kode = j.KodeHalte
	JOIN dbo.Bis b
		ON b.Kode = j.KodeBis
	GROUP BY
		h.Kode

SELECT DISTINCT
	h.Kode KodeHalte,
	h.Nama NamaHalte,
	sm.BanyakBis
FROM dbo.Jalur j
JOIN dbo.Halte h
	ON h.Kode = j.KodeHalte
JOIN dbo.Bis b
	ON b.Kode = j.KodeBis
JOIN no09 sm ON sm.KodeHalte = h.Kode
WHERE
	sm.BanyakBis = (SELECT MAX(BanyakBis) FROM no09)

--10 Tampilkan kolom Keterangan pada tabel Transaksi: jika tarif = 0 maka Saldo Tidak Terpotong, jika tarif != 0 maka Saldo Terpotong

SELECT
	trs.*,
	CASE
		WHEN trf.Harga = 0 THEN 'Saldo Tidak Terpotong'
		WHEN trf.Harga != 0 THEN 'Saldo Terpotong'
	END Keterangan
FROM dbo.Transaksi trs
JOIN dbo.Tarif trf
	ON trf.Kode = trs.KodeTarif
