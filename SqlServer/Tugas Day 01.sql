--Soal SQL
--Note jumlah penjualan = qty * hrg, jumlah produk = qty


--01 Tampilkan jumlah penjualan barang peroutlet per-tanggal
select day(SellingDate) [Selling Date], outlet.Nama[Nama Kota], outlet.Kode, s.Quantity*product.Harga [Jumlah Penjualan]
from Selling s
join Outlet outlet on outlet.Kode = s.KodeOutlet
join Product product on product.Kode = s.KodeProduct
order by SellingDate

--02 Tapilkan jumlah penjualan per tahun
select year(SellingDate) [Selling Date], outlet.Nama[Nama Kota], outlet.Kode, s.Quantity*product.Harga [Jumlah Penjualan]
from Selling s
join Outlet outlet on outlet.Kode = s.KodeOutlet
join Product product on product.Kode = s.KodeProduct
order by SellingDate

--03 Tampilkan jumlah product terlaris dan ter tidak laris per kota
select 
	result.KodeKota,
	result.NamaKota,
	MAX(result.Quantity) [Terlaris],
	MIN(result.Quantity) [Tidak laris]
from
(
select sell.KodeProduct, kota.Kode KodeKota, SUM(sell.Quantity) [Quantity], kota.Nama NamaKota
from Product product
join Selling sell on sell.KodeProduct = product.Kode
join Outlet outlet on outlet.Kode = sell.KodeOutlet
join Kota kota on kota.Kode = outlet.KodeKota
group by kota.Kode, sell.KodeProduct, kota.Nama
) result
group by result.KodeKota, result.NamaKota
order by result.KodeKota

select * from Kota

--04 Tampilkan jumlah penjualan per provinsi dan urutkan dari yang terbesar
select provinsi.Kode [Kode Provinsi],  sum(s.Quantity*product.Harga) [Jumlah Penjualan]
from Selling s
join Outlet outlet on outlet.Kode = s.KodeOutlet
join Kota kota on kota.Kode = outlet.KodeKota
join Provinsi provinsi on provinsi.Kode = kota.KodeProvinsi
join Product product on product.Kode = s.KodeProduct
group by provinsi.Kode
order by [Jumlah Penjualan] desc

--05 Tampilan referensi yang tidak sesuai dengan sellingdate
SELECT Reference, SellingDate
FROM 
	(SELECT Referensi Reference,
	SellingDate,
	YEAR(SellingDate) SelYear, 
	MONTH(SellingDate) SelMonth, 
	CAST(SUBSTRING(Referensi, 6,2)as int) dtRef, 
	CAST(SUBSTRING(Referensi, 4,2)as int) yrRef 
FROM Selling) sel
WHERE  sel.SelMonth <> sel.dtRef AND SelYear <> yrRef

--Atau

SELECT Referensi reference, SellingDate
FROM Selling
WHERE
	CAST(SUBSTRING(Referensi, 4,2) as int) <> YEAR(SellingDate)
	AND CAST(SUBSTRING(Referensi, 6,2) as int) <> MONTH(SellingDate)

--06 Tampilan jumlah produk terjual pertahun peroutlet
select year(sell.SellingDate) [Tahun],  outlet.Kode[Kode Outlet], outlet.Nama[Outlet], SUM(sell.Quantity) [Quantity]
from Selling sell
join Outlet outlet on outlet.Kode = sell.KodeOutlet
group by outlet.Kode,  outlet.Nama, year(sell.SellingDate)

--07 Tampilan jumlah penjualan peroutlet
select outlet.Kode[Kode Outlet], outlet.Nama[Outlet], sum(sell.Quantity*product.Harga) [Jumlah Penjualan]
from Selling sell
join Outlet outlet on outlet.Kode = sell.KodeOutlet
join Product product on product.Kode = sell.KodeProduct
group by outlet.Kode,  outlet.Nama

--08 Tampilan jumlah penjualan per bulan diurutkan berdasar bulan
select month(sell.SellingDate) [Bulan], sum(sell.Quantity*product.Harga) [Jumlah Penjualan]
from Selling sell
join Outlet outlet on outlet.Kode = sell.KodeOutlet
join Product product on product.Kode = sell.KodeProduct
group by month(sell.SellingDate), YEAR(sell.SellingDate)
order by YEAR(sell.SellingDate)

--09 Tampilan rata-rata jumlah penjualan setiap bulan
select month(sell.SellingDate) [Bulan], avg(sell.Quantity*product.Harga) AvgPenjualan
from Selling sell
join Outlet outlet on outlet.Kode = sell.KodeOutlet
join Product product on product.Kode = sell.KodeProduct
group by month(sell.SellingDate)

--10 Tampilan produk dengan jumlah produk dibawah rata-rata
select Kode, Nama, sum(Quantity)[Dibawah rata-rata]
from Product
join Selling on KodeProduct = Product.Kode
group by Nama, Kode
having SUM(Quantity) > (
	select 
		AVG(JumlahPenjualan) rataRata
	from
	(
		select
			SUM(quantity) JumlahPenjualan
		from Selling
		group by Selling.KodeProduct
	) TotalPerProduk
)
AND Not Nama LIKE '%i'

--11 Tampilan jumlah outlet per provinsi
select Provinsi.Nama, COUNT(KodeKota) JumlahOutlet
from Outlet
join Kota on Kota.Kode = Outlet.KodeKota
join Provinsi on Provinsi.Kode = Kota.KodeProvinsi
group by Provinsi.Nama


--12 Tampilan produk terlaris per perionde bulanan
--Cara Bapa
select SelQty.*
from SelQty
join
	(
		select [Month], MAX(Quantity) Quantity
		from
			SelQty
		group by [Month]
	)selMaxQty
on SelQty.Quantity = selMaxQty.Quantity and selQty.Month = selMaxQty.Month
order by SelQty.Month

create view selQty
as
select MONTH(SellingDate) [Month], KodeProduct, SUM(Quantity) Quantity
from Selling
group by MONTH(SellingDate), KodeProduct

--cara aku tapi ga belum dapet kalau nampilin kode product nya
--select result.Bulan Bulan, MAX(result.JumlahProduk) JumlahProduk
--from(
--	select Month(Selling.SellingDate) Bulan, KodeProduct KodeProduk, sum(Quantity) JumlahProduk
--	from Selling
--	join Product on Selling.KodeProduct = Product.Kode
--	group by MONTH(Selling.SellingDate), KodeProduct
--) result
--group by result.Bulan




--13 Tampilkan provinsi yg menjual sampoo, quantity, harga & jumlah penjualan
select Provinsi.Kode, Provinsi.Nama, SUM(sell.Quantity) Kuantitas, Product.Harga, SUM(sell.Quantity*Harga) JumlahPenjualan
from Provinsi
join Kota on Kota.KodeProvinsi = Provinsi.Kode
join Outlet on Outlet.KodeKota = Kota.Kode
join Selling sell on sell.KodeOutlet = Outlet.Kode
join Product on Product.Kode = sell.KodeProduct
where Product.Kode = 'P1022'
group by Provinsi.Kode, Provinsi.Nama, Product.Harga



--14 Tampilkan produk harga termahal setiap provinsi
select pmhPro.NamaProv, pmhPro.NamaProduk, pmhPro.Harga
from ProvMaxHarga pmhPro
	join 
	(
	select KodeProv, NamaProv, MAX(Harga) Harga
		from ProvMaxHarga
		group by KodeProv, NamaProv
	)pmh
	on pmhPro.KodeProv = pmh.KodeProv and  pmhPro.Harga = pmh.Harga

create view ProvMaxHarga
as
select Provinsi.Kode KodeProv, Provinsi.Nama NamaProv, Product.Nama NamaProduk, max(Product.Harga) Harga
from Product
join Selling sell on sell.KodeProduct = Product.Kode
join Outlet on Outlet.Kode = sell.KodeOutlet
join Kota on Kota.Kode = Outlet.KodeKota
join Provinsi on Provinsi.Kode = Kota.KodeProvinsi
group by Provinsi.Kode, Provinsi.Nama, Product.Nama


--15 Tampilkan outlet dengan penjualan tertinggi & terendah
SELECT
		CASE
			WHEN sm.Penjualan = (SELECT * FROM max15) THEN 'Maksimal'
			WHEN sm.Penjualan = (SELECT * FROM min15) THEN 'Minimal'
		END Stat,
		sm.NamaO NamaOutlet,
		sm.Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			o.Nama NamaO,
			--prod.Kode KodeP,
			ISNULL(SUM(s.Quantity * prod.Harga),0) Penjualan
		FROM dbo.Selling s
		RIGHT JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode,
			o.Nama
	) sm
	WHERE
		sm.Penjualan = (SELECT * FROM max15) OR
		sm.Penjualan = (SELECT * FROM min15)
		--sm.Penjualan = @max OR
		--sm.Penjualan = @min

CREATE VIEW max15
AS
	SELECT
		MAX(mx.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mx

ALTER VIEW min15
AS
	SELECT
		MIN(mn.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			ISNULL(SUM(s.Quantity * prod.Harga), 0) Penjualan
		FROM dbo.Outlet o
		LEFT JOIN dbo.Selling s
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mn

SELECT MIN(Penjualan) Penjualan FROM
(SELECT
	o.Kode KodeO,
	--prod.Kode KodeP,
	ISNULL(SUM(s.Quantity * prod.Harga), 0) Penjualan
FROM dbo.Outlet o
LEFT JOIN dbo.Selling s
	ON o.Kode = s.KodeOutlet
LEFT JOIN dbo.Product prod
	ON prod.Kode = s.KodeProduct
GROUP BY
	o.Kode) MinO
--GROUP BY KodeO


select * from Kota
select * from Outlet
select * from Product
select * from Provinsi
select * from Selling


